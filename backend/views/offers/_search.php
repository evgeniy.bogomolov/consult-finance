<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use backend\models\Clients;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offers-filters">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="row">
	    <?= $form->field($model, 'city', ['options'=>['class'=>'col-md-2']])->dropDownList($cities, ['prompt'=>''])->label("Город") ?>
        <?= $form->field($model, 'status', ['options'=>['class'=>'col-md-2']])->dropDownList($statuses, ['prompt'=>''])->label("Статус") ?>

        <?= $form->field($model, 'date', ['options'=>['class'=>'col-md-2']])->textInput(
            [
              'id'=>'date',
                'autocomplete'=>'off'
            ]) ?>

        <?php
            $clients = Clients::find()
                ->select(['id as value', 'concat(phone," ",name) as label'])
                ->asArray()
                ->all();

        ?>
    <?= $form->field($model, 'client_id', ['options'=>['class'=>'col-md-2']])->widget(
            AutoComplete::className(), [
            'clientOptions' => [
                'source' => $clients,
                'delay' => 1000
            ],
            'options'=>[
                'class'=>'form-control'
            ]
        ])->label('Клиент'); ?>

        <?= $form->field($model, 'responsible', ['options'=>['class'=>'col-md-2']])->dropDownList($responsibles, ['prompt'=>''])->label("Ответственный") ?>

    <?php // echo $form->field($model, 'division') ?>

    <?php // echo $form->field($model, 'responsible') ?>

    <?php // echo $form->field($model, 'source') ?>

    <?php // echo $form->field($model, 'credit_summ') ?>

    <?php // echo $form->field($model, 'comment') ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Сброс',['index'],['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
