<?php

use yii\db\Migration;

/**
 * Class m190411_152532_add_columns_to_offers_table
 */
class m190411_152532_add_columns_to_offers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%offers}}', 'city', $this->integer());
        $this->addColumn('{{%offers}}', 'division', $this->integer());
        $this->addColumn('{{%offers}}', 'responsible', $this->integer());
        $this->addColumn('{{%offers}}', 'source', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190411_152532_add_columns_to_offers_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190411_152532_add_columns_to_offers_table cannot be reverted.\n";

        return false;
    }
    */
}
