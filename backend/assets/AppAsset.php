<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css',
        'css/site.css',
        'css/jquery.fancybox.min.css',
        'css/base.css'
    ];
    public $js = [
        'js/jquery.autocomplete.js',
        'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js',
        'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js',
        'js/bootstrap.file-input.js',
        'js/notify.min.js',
        'js/jquery.inputmask.min.js',
        'js/jquery.fancybox.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
