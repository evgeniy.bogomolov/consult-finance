<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\offers */

$this->title = 'Редактирование сделки: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Сделки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="offers-update">

    <?= $this->render('_form', [
        'model' => $model,
        'clients' => $clients,
        'statuses' => $statuses,
        'cities' => $cities,
        'departments' => $departments,
        'users' => $users,
        'sources' => $sources,
        'isUpdate' => true
    ]) ?>

</div>
