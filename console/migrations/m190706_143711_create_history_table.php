<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%history}}`.
 */
class m190706_143711_create_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%history}}', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime(),
            'offer_id' => $this->integer(),
            'action' => $this->string(),
            'user_id' => $this->integer(),
            'payment_id' => $this->integer(),
            'service_id' => $this->integer(),
            'department_id' => $this->integer(),
            'old_status_id' => $this->integer(),
            'new_status_id' => $this->integer(),
            'old_responsible_id' => $this->integer(),
            'new_responsible_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%history}}');
    }
}
