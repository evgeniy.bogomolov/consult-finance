<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "clientsFiles".
 *
 * @property int $id
 * @property string $file
 * @property string $client_id
 */
class ClientsFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientsFiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'string', 'max' => 255],
            [['client_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'File',
            'client_id' => 'Client ID',
        ];
    }
}
