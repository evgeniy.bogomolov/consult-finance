<?php
namespace backend\controllers;

use backend\models\Cities;
use backend\models\Departments;
use backend\models\UserCities;
use common\models\PasswordChangeForm;
use common\models\User;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use yii\web\UploadedFile;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'password-change', 'delete-photo'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['update-other', 'block', 'unblock', 'password-change-other'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'model' => $this->findModel(),
        ]);
    }

    public function actionUpdate()
    {
        $model = $this->findModel();

        $departments = ArrayHelper::map(Departments::find()->all(), "id", "name");
        $cities = ArrayHelper::map(Cities::find()->all(), "id", "city");

        if ($model->load(Yii::$app->request->post())) {
	        if(UploadedFile::getInstance($model, 'user_photo')) {
		        $model->user_photo = UploadedFile::getInstance($model, 'user_photo');
		        $model->upload();
	        }
	        $model->save();
            return $this->redirect(['index']);
        } else {

            return $this->render('update', [
                'model' => $model,
                'departments' => $departments,
                'cities' => $cities
            ]);
        }
    }

    public function actionUpdateOther($id) {
        $model = User::findOne($id);
        $model->cities_ids = $model->cities;

        if ($model->load(Yii::$app->request->post())) {
			if(UploadedFile::getInstance($model, 'user_photo')) {

				$model->user_photo = UploadedFile::getInstance($model, 'user_photo');
				$model->upload();
			}


	        $model->saveOther();
            return $this->redirect(['site/users']);
        }

        $departments = ArrayHelper::map(Departments::find()->all(), "id", "name");
        $cities = ArrayHelper::map(Cities::find()->all(), "id", "city");
        $roles = ArrayHelper::map(Yii::$app->getAuthManager()->getRoles(), 'name', 'description');
        $model->role = array_keys(Yii::$app->authManager->getRolesByUser($model->id))[0];

        return $this->render('update-other', [
            'model' => $model,
            'departments' => $departments,
            'cities' => $cities,
            'roles' => $roles
        ]);
    }

    public function actionDeletePhoto($id) {
		$model = User::findOne($id);
		$model->photo = false;
		$model->save();
		if(Yii::$app->user->can('admin')) {
			return $this->redirect(['profile/update-other', 'id' => $id]);
		} else {
			return $this->redirect(['profile/update']);
		}

	}

    public function actionBlock($id) {
        $model = User::findOne($id);

        $model->status = User::STATUS_INACTIVE;

        $model->save();

        Yii::$app->session->setFlash('success', "Пользователь заблокирован");

        return $this->redirect(['site/users']);
    }

    public function actionUnblock($id) {
        $model = User::findOne($id);

        $model->status = User::STATUS_ACTIVE;

        $model->save();

        Yii::$app->session->setFlash('success', "Пользователь разблокирован");

        return $this->redirect(['site/users']);
    }

    public function actionPasswordChange()
    {
        $user = $this->findModel();
        $model = new PasswordChangeForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('passwordChange', [
                'model' => $model,
            ]);
        }
    }

    public function actionPasswordChangeOther($id)
    {
        $model = User::findOne($id);
        if(Yii::$app->request->post()) {
            $model->setPassword(Yii::$app->request->post("new_password"));
            if($model->save()) {
                Yii::$app->session->setFlash('success', "Успешно изменен пароль для пользователя $model->username");
            } else {
                Yii::$app->session->setFlash('success', "Ошибка. Пароль не изменен.");
            }

            return$this->redirect(['site/users']);
        }

        return $this->render('password-change-other', [
            'model' => $model,
        ]);
    }


    /**
     * @return User the loaded model
     */
    private function findModel()
    {
        return User::findOne(Yii::$app->user->identity->getId());
    }
}