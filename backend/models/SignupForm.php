<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $fullname;
    public $phone;
    public $birthday;
    public $email;
    public $password;
    public $role;
    public $city_id;
    public $department_id;
    public $cities_ids;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['fullname', 'string', 'min' => 2, 'max' => 255],
            ['birthday', 'string', 'min' => 2, 'max' => 255],
            ['role', 'string', 'min' => 2, 'max' => 255],
            ['cities_ids', 'safe'],
            ['phone', 'string', 'min' => 2, 'max' => 255],
            ['city_id', 'integer'],
            ['department_id', 'integer'],
            ['department_id', 'required'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->fullname = $this->fullname;
        $user->phone = $this->phone;
        $user->birthday = $this->birthday;
        $user->email = $this->email;
        $user->city_id = $this->city_id;
        $user->department_id = $this->department_id;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        $user->generateEmailVerificationToken();
        $saved = $user->save();

        if($saved) {
            $userRole = Yii::$app->authManager->getRole($this->role);
            Yii::$app->authManager->assign($userRole, $user->id);
            foreach ($this->cities_ids as $user_city) {
                $user_cities = new UserCities();
                $user_cities->user_id = $user->id;
                $user_cities->cities_id = $user_city;
                $user_cities->save();
            }

        }

        return $saved && $this->sendEmail($user);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }

    public function attributeLabels()
    {
        return [
            'fullname' => 'Полное имя',
            'phone' => 'Телефон',
            'birthday' => 'День рождения',
            'role' => 'Роль',
            'password' => 'Пароль',
            'city_id' => 'Город',
            'department_id' => 'Отдел'
        ];
    }
}
