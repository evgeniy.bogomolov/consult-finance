<?php

namespace backend\models;

use function GuzzleHttp\Psr7\str;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Offers;

/**
 * OffersSearch represents the model behind the search form of `backend\models\Offers`.
 */
class OffersSearch extends Offers
{
    public $date_period;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'city', 'division', 'responsible', 'source'], 'integer'],
            [['date', 'status', 'credit_summ', 'comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $date_from = null;
        $date_to = null;

        if($params["OffersSearch"]["date"]) {
            $date_period = explode(" - ", $params["OffersSearch"]["date"]);
            $date_from = $date_period[0];
            $date_to = $date_period[1];
        }

        $available_offers['division'] = [];

        if(Yii::$app->user->can('viewCCOffers')) {
            $available_offers['division'][] = 6;
        }
        if(Yii::$app->user->can('viewSDOffers')) {
            $available_offers['division'][] = 7;
        }
        if(Yii::$app->user->can('viewEDOffers')) {
            $available_offers['division'][] = 8;
        }

        if(!Yii::$app->user->can('viewAllCitiesOffers')) {
            $current_user = Yii::$app->user->getIdentity();
            $current_user_cities = $current_user->cities;
            foreach ($current_user_cities as $current_user_city) {
                $available_offers['city'][] = $current_user_city->id;
            }
        }

        if($params["OffersSearch"]["status"] && $params["OffersSearch"]["date"] && $params["OffersSearch"]["status"] != 7) {

            $statusDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');
            $statusdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to. ' 23:59:59');

            $history = History::find()
                ->where(['action' => 'change_status', 'new_status_id'=>$params["OffersSearch"]["status"]])
                ->andFilterWhere(['>=', 'date', $date_from ? $statusDateFrom->format('Y-m-d H:i:s') : null])
                ->andFilterWhere(['<=', 'date', $date_to ? $statusdDateTo->format('Y-m-d H:i:s') : null])
                ->all();
            foreach ($history as $history_item) {
                $available_offers['id'][] = $history_item->offer_id;
            }
        }

        if($params["OffersSearch"]["status"] && !$params["OffersSearch"]["date"] && $params["OffersSearch"]["status"] != 7) {
	        $available_offers['id'] = Offers::find()
	                                        ->where(["status" => $params["OffersSearch"]["status"]])
	                                        ->select('id')
	                                        ->asArray()
	                                        ->column();
        }

        if($params["OffersSearch"]["status"] == 7) {

            $statusDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');
            $statusdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to. ' 23:59:59');

            $meeting = Action::find()
                ->where(['name'=>'meeting'])
                ->andFilterWhere(['>=', 'datetime', $date_from ? $statusDateFrom->format('Y-m-d H:i:s') : null])
                ->andFilterWhere(['<=', 'datetime', $date_to ? $statusdDateTo->format('Y-m-d H:i:s') : null])
                ->all();

            foreach ($meeting as $meeting_item) {
                $available_offers['id'][] = $meeting_item->offer_id;

            }
        }


        $query = Offers::find()->where($available_offers);


        $query->orderBy(['date'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $date_period = explode(" - ", $this->date);
        $date_from = $date_period[0];
        $date_to = $date_period[1];

        $createdDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');

        $createdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to. ' 23:59:59');

        if(!$params["OffersSearch"]["status"]) {
            $query
                ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
                ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null]);
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'city' => $this->city,
            'division' => $this->division,
            'responsible' => $this->responsible,
            'source' => $this->source,
        ]);

        /*$query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'credit_summ', $this->credit_summ])
            ->andFilterWhere(['like', 'comment', $this->comment]);*/

        return $dataProvider;
    }
}
