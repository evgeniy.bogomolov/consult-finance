<?php

namespace backend\models;

use common\models\User;
use Symfony\Component\BrowserKit\History;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use backend\models\History as OfferHistory;
/**
 * This is the model class for table "Offers".
 *
 * @property int $id
 * @property int $client_id
 * @property string $date
 * @property string $status
 * @property int $city
 * @property int $division
 * @property int $responsible
 * @property int $source
 * @property string $credit_summ
 * @property string $comment
 * @property string $updated_at
 */
class Offers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers';
    }

    public function afterSave($insert, $changedAttributes)
    {
       /* if ($insert) {
            Yii::$app->session->setFlash('success', 'Запись добавлена');
        } else {
            Yii::$app->session->setFlash('success', 'Запись обновлена');
        }*/

        if ($insert) {
            $history = new OfferHistory();
            $history->offer_id = $this->id;
            $history->user_id = Yii::$app->user->identity->id;

            $history->action = "created";
            $history->save();
        }

       if(isset($changedAttributes['division']) && $changedAttributes['division'] != $this->division) {
           $history = new OfferHistory();
           $history->offer_id = $this->id;
           $history->user_id = Yii::$app->user->identity->id;

           $history->old_department_id = $changedAttributes['division'];
           $history->new_department_id = $this->division;
           $history->action = "change_department";

           $history->save();
       }

       if(isset($changedAttributes['status']) && $changedAttributes['status'] != $this->status) {
           $history = new OfferHistory();
           $history->offer_id = $this->id;
           $history->user_id = Yii::$app->user->identity->id;

            $history->old_status_id = $changedAttributes['status'];
            $history->new_status_id = $this->status;
            $history->action = "change_status";

           $history->save();
       }

        if(array_key_exists('responsible', $changedAttributes) && $changedAttributes['responsible'] != $this->responsible) {
            $history = new OfferHistory();
            $history->offer_id = $this->id;
            $history->user_id = Yii::$app->user->identity->id;
            $history->old_responsible_id = $changedAttributes['responsible'] ? $changedAttributes['responsible'] : 0;
            $history->new_responsible_id = $this->responsible;
            $history->action = "change_responsible";

            $history->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->status == 13) {
                $this->division = 8;
            }
            if($this->status == 26) {
                $this->division = 7;
            }

            return true;
        }
        return false;
    }

    public function behaviors(){
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'city', 'division', 'responsible'], 'integer'],
            [['date'], 'safe'],
            [['updated_at'], 'safe'],
            [['status', 'credit_summ'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => '25'],
            [['comment'], 'string'],
            [['source'], 'default', 'value' => '2'],
            [['division'], 'default', 'value' => '6']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'ID Клиента',
            'date' => 'Дата',
            'status' => 'Статус',
            'city' => 'Город',
            'division' => 'Отдел',
            'responsible' => 'Ответственный',
            'source' => 'Источник',
            'credit_summ' => 'Сумма кредита',
            'comment' => 'Комментарий'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    public function getStatusname()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    public function getCityname()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'division']);
    }

    public function getResponsibleName()
    {
        return $this->hasOne(User::className(), ['id' => 'responsible']);
    }

    public function getSourcename()
    {
        return $this->hasOne(Source::className(), ['id' => 'source']);
    }

    public function getServices()
    {
        return $this->hasMany(Services::className(), ['offer_id' => 'id']);
    }

    public function getActions()
    {
        return $this->hasMany(Action::className(), ['offer_id' => 'id'])->orderBy(['datetime'=>SORT_DESC]);
    }

    public function getHistory()
    {
        return $this->hasMany(OfferHistory::className(), ['offer_id' => 'id'])->orderBy(['date'=>SORT_DESC]);
    }

    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['offer_id' => 'id']);
    }
}
