<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\status */

$this->title = 'Добавить статус';
$this->params['breadcrumbs'][] = ['label' => 'Статусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-create">

    <?= $this->render('_form', [
        'model' => $model,
        'departments' => $departments
    ]) ?>

</div>
