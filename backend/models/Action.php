<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "actions".
 *
 * @property int $id
 * @property string $name
 * @property string $datetime
 * @property string $address
 * @property int $offer_id
 * @property int $user_id
 */
class Action extends \yii\db\ActiveRecord
{
    const MEETING = "meeting";
    const CALLBACK = "callback";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'actions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['datetime'], 'date', 'format' => 'php:d-m-Y H:i'],
            [['datetime'], 'string'],
            [['offer_id', 'user_id'], 'integer'],
            [['name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'datetime' => 'Время',
            'address' => 'Адрес',
            'offer_id' => 'Offer ID',
            'user_id' => 'User ID',
        ];
    }
}
