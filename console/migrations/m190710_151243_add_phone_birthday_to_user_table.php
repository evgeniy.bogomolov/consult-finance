<?php

use yii\db\Migration;

/**
 * Class m190710_151243_add_phone_birthday_to_user_table
 */
class m190710_151243_add_phone_birthday_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'phone', $this->string());
        $this->addColumn('{{%user}}', 'birthday', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190710_151243_add_phone_birthday_to_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190710_151243_add_phone_birthday_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
