<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PaymentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]); ?>
<div class="offers-filters">
    <div class="container-fluid no-paddings">
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'city_id')->dropDownList($filters['cities'], ['prompt'=>'Все']) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'department_id')->dropDownList($filters['departments'], ['prompt'=>'Все']) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'user_id')->dropDownList($filters['users'], ['prompt'=>'Все']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'date')->textInput(
                    [
                        'id'=>'date',
                        'autocomplete' => 'off',
                    ])->label("Период") ?>
            </div>
            <div class="col-md-3">
                <div class="form-group" style="padding-top: 24px">
                    <?= Html::submitButton('Применить', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Сброс',['index'],['class' => 'btn btn-warning']) ?>
                </div>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>

