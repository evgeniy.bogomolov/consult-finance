<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\offers */

$this->title = "Сделка #".$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Сделки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$service_names = [
    "ЛБ"=>"ЛБ",
    "ПГБ" => "ПГБ",
    "Доплата до лб" => "Доплата до лб",
    "Фо" => "Фо",
    "Ипотека" => "Ипотека",
    "Бфл" => "Бфл",
];

\yii\web\YiiAsset::register($this);
?>
<div class="offers-view">

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if(Yii::$app->user->can("admin")): ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php endif; ?>
    </p>


</div>

<div class="offer">
    <?php $form = ActiveForm::begin(); ?>
    <a href="<?=Yii::$app->urlManager->createUrl(['clients/view', 'id' => $model->client->id])?>" class="offer-contact-wrap">
    <div class="offer-contact">
        <div class="offer-contact-title h3">Контакт</div>
        <div class="offer-contact-name"><?= $model->client->name ?></div>
        <div class="offer-contact-phone"><?= $model->client->phone ?></div>
    </div>
    </a>
    <div class="offer-info">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?php
                    if(Yii::$app->user->can('changeCity')) {
                        echo $form->field($model, 'city')->dropDownList($cities, ['class' => 'form-control']);
                    } else {
                        echo $form->field($model, 'city')->dropDownList($cities, ['class' => 'form-control', 'disabled' => 'disabled']);
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">

                        <?php if(Yii::$app->user->can('change_department')): ?>
                            <?php
                            echo $form->field($model, 'division')->dropDownList($departments, ['class'=>'form-control']);
                            ?>
                        <?php else: ?>
                            <div class="form-group">
                                <label class="control-label" for="offers-division">Отдел</label>
                                <strong><?= $model->department->name ?></strong>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="form-group">

                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <a href="#" data-offer-id="<?= $model->id ?>" class="be-responsible">Стать ответственным</a>
                    <?php if(Yii::$app->user->can('setResponsibles')): ?>
                        <?php
                        echo $form->field($model, 'responsible')->dropDownList($users, ['class'=>'form-control', 'prompt' => 'Не назначен']);
                        ?>
                    <?php else: ?>
                        <div class="form-group">
                            <label class="control-label">Ответственный</label>
                            <?php
                            echo "<strong>";
                            echo $model->responsibleName->fullname ? $model->responsibleName->fullname : "Не назначен";
                            echo "</strong>";
                            ?>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Сумма кредита</label>
                    <?= $model->credit_summ ? $model->credit_summ : "Не задано" ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?php
                    echo $form->field($model, 'source')->dropDownList($sources, ['class'=>'form-control', 'disabled'=>'disabled']);
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?php
                    echo $form->field($comment, 'comment')->textarea(['class'=>'form-control']);
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <?php
                echo $form->field($model, 'status')->dropDownList($statuses, ['class'=>'form-control', "prompt" => "---"]);
                ?>

            </div>
            <div class="col-md-3">
                <?= Html::submitButton('Обновить сделку', ['class' => 'btn btn-success', 'style' => 'margin-top: 24px']) ?>
            </div>
        </div>
    </div>



    <div class="row">
        <?php if(Yii::$app->user->can('addServices')): ?>
        <div class="col-md-6">
            <div class="offer-services">
                <div class="offer-services-title h3">
                    Услуги
                </div>
                <div class="offer-services-head">
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="offer-service-name">Услуга</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="offer-service-paid">Оплачено</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="offer-service-amount">Стоимость</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="offer-service-amount">Действия</div>
                        </div>
                    </div>
                </div>
                <div class="offer-services-list">
                <?php foreach ($model->services as $service): ?>
                    <?php
                        $payments = $service->payments;
                        $service_paid = 0;
                        foreach ($payments as $payment) {
                            $service_paid += $payment->summ;
                        }
                        $paid_text = number_format($service_paid, 0, '', ' ');
                    ?>
                    <div class="offer-service">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="offer-service-name"><?= $service->name ?></div>
                            </div>
                            <div class="col-xs-3">
                                <div class="offer-service-paid text-green">
                                    <span class="paid_text"><?= $paid_text ?></span>
                                    <a href="/offers/view-payments/?service_id=<?= $service->id ?>" class="view-all-payments" title="Все платежи по услуге" aria-label="Все платежи по услуге">
                                        <span class="glyphicon glyphicon-list"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="offer-service-amount">
                                    <span class="offer-service-amount-text">
                                        <?= number_format($service->cost, 0, '', ' '); ?>
                                    </span>
                                    <?php if(Yii::$app->user->can("canChangeServicePrice")): ?>
                                    <a href="#" class="service-amount-update-button" title="Изменить" aria-label="Изменить" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="offer-service-payment"><a href="#" class="add-payment-button">+ добавить платеж</a></div>
                            </div>
                        </div>
                        <?php if(Yii::$app->user->can("canChangeServicePrice")): ?>
                        <div class="service-amount-update modal fade">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Изменить стоимость</h4>
                                    </div>
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label for="">Старая цена</label>
                                            <input type="text" name="old_summ" disabled="disabled" value="<?= number_format($service->cost, 0, '', ' '); ?>" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Новая цена</label>
                                            <input type="text" name="new_summ" class="form-control">
                                        </div>
                                        <input type="hidden" name="service_id" value="<?= $service->id ?>">

                                        <div class="form-group">
                                            <button class="amount_update_button btn btn-success">Изменить</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="add-payment modal fade">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Добавить платеж</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="">Способ оплаты</label>
                                            <?= Html::dropDownList("method_id","null", $payment_methods,["class"=>"form-control"] ) ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Сумма платежа</label>
                                            <input type="text" name="summ" placeholder="1000" class="form-control">

                                            <input type="hidden" name="service_id" value="<?= $service->id ?>">
                                            <input type="hidden" name="city_id" value="<?= $model->city ?>">

                                        </div>
                                        <div class="form-group">
                                            <button class="add_payment_button btn btn-success">Добавить</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
                </div>
                <div class="offer-service-add-service">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn btn-default add-service-button">Добавить услугу</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="col-md-6">
            <div class="comments__history">
                <div class="h3">
                    Комментарии
                </div>
                <?php foreach ($model->comments as $comment): ?>
                <div class="row">
                    <div class="col-md-5">
                        <?= date('d.m.Y', strtotime($comment->date)); ?> <br>
                        <?= $comment->user->fullname ?>
                        (<?= $comment->user->department->name ?>)
                    </div>
                    <div class="col-md-7">
                        <?= $comment->comment ?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>


    </div>

    <?php ActiveForm::end(); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="offer-add-task">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#panel1">Встреча</a></li>
                    <li><a data-toggle="tab" href="#panel2">Перезвонить</a></li>
                </ul>

                <div class="tab-content">
                    <div id="panel1" class="tab-pane fade in active">
                        <?php $MeetForm = ActiveForm::begin([
                            'options' => ['class' => 'add_action'],
                        ]); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php
                                    echo $MeetForm->field($actionForm, 'datetime')->textInput(['class'=>'form-control', "id"=>"meet-time"]);
                                    ?>

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php
                                    echo $MeetForm->field($actionForm, 'address')->dropDownList($addresses, ['class'=>'form-control']);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-success add_action_button">Применить</button>
                                </div>
                            </div>
                        </div>
                        <?= $MeetForm->field($actionForm, 'name')->hiddenInput(['value'=>'meeting'])->label(false); ?>
                        <?= $MeetForm->field($actionForm, 'offer_id')->hiddenInput(['value'=> $model->id])->label(false); ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div id="panel2" class="tab-pane fade">
                        <?php $RingForm = ActiveForm::begin([
                            'options' => ['class' => 'add_action'],
                        ]); ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php
                                    echo $RingForm->field($actionForm, 'datetime')->textInput(['class'=>'form-control', "id"=>"ring-time"]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-success add_action_button">Применить</button>
                                </div>
                            </div>
                        </div>
                        <?= $RingForm->field($actionForm, 'name')->hiddenInput(['value'=>'callback'])->label(false); ?>
                        <?= $RingForm->field($actionForm, 'offer_id')->hiddenInput(['value'=> $model->id])->label(false); ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="offer-current-task">
                <div class="row">
                    <div class="col-md-12">
                        <div class="h3">Задачи</div>
                    </div>
                </div>
                <div class="offer-task-list">
                    <?foreach ($model->actions as $action):?>
                        <?php
                            $past = time() > strtotime($action->datetime) ? 'past' : '';
                        ?>
                    <div class="offer-task <?= $past ?>">
                        <div class="row">
                            <div class="col-md-2">
                                <?php
                                switch ($action->name){
                                    case 'meeting':
                                        echo 'Встреча';
                                        break;
                                    case 'callback':
                                        echo 'Перезвонить';
                                        break;
                                }
                                ?>
                            </div>
                            <div class="col-md-5">
                                <?= date('d.m.Y H:i', strtotime($action->datetime)); ?>
                            </div>
                            <div class="col-md-5">
                                <?= $action->address; ?>
                            </div>
                        </div>
                    </div>
                    <?endforeach;?>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="offer-history">
                <div class="h3">
                    История
                </div>
                <?php foreach ($history as $history_item): ?>
                    <div class="row">
                        <div class="col-xs-4">
                            <?= $history_item->date ?> <br>
                            <?= $history_item->user->fullname ?>
                        </div>
                        <?php if($history_item['action'] == 'created'): ?>
                            <div class="col-xs-8">
                                <div class="new">
                                    Сделка создана
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($history_item['action'] == 'change_department'): ?>
                            <div class="col-xs-4">
                                <div class="old">
                                    <?= $history_item->oldDepartment->name ?>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="new">
                                    <?= $history_item->newDepartment->name ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($history_item['action'] == 'change_status'): ?>
                            <div class="col-xs-4">
                                <div class="old">
                                    <?= $history_item->oldStatus->status_name ?>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="new">
                                    <?= $history_item->newStatus->status_name ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($history_item['action'] == 'change_responsible'): ?>
                            <div class="col-xs-4">
                                <div class="old">
                                    <?= $history_item->oldResponsible->fullname ? $history_item->oldResponsible->fullname : "Назначен ответственный" ?>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="new">
                                    <?= $history_item->newResponsible->fullname ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($history_item['action'] == 'add_payment'): ?>
                            <div class="col-xs-4">
                                <div class="new">
                                    +<?= $history_item->payment->summ ?> (<?= $history_item->service->name ?>)
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <?= $history_item->payment->responsible->fullname ?>
                            </div>
                        <?php endif; ?>

                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>



<div class="add-service modal fade">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Добавить услугу</h4>
            </div>
            <div class="modal-body">

                <?php $form = ActiveForm::begin([
                    'action' => \yii\helpers\Url::to(['offers/add-service', 'id'=>$model->id]),
                    'method' => 'post',
                    'options' => ['class' => 'add_service_form', 'target' => '_blank'],
                ]); ?>

                <?php
                echo $form->field($serviceForm, 'name')->dropDownList($service_names, ['class'=>'form-control']);
                echo $form->field($serviceForm, 'cost')->textInput(['class'=>'form-control', 'placeholder' => '10000']);
                echo $form->field($serviceForm, 'department_id')->hiddenInput(['class'=>'form-control', 'value'=>$model->division])->label(false);
                echo $form->field($serviceForm, 'offer_id')->hiddenInput(['class'=>'form-control', 'value'=>$model->id])->label(false);
                echo Html::submitButton('Добавить', ['class' => 'btn btn-success add_service_button'])
                ?>

                <? ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="all-payments modal fade">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Платежи</h4>
            </div>
            <div class="modal-body">


            </div>
        </div>
    </div>
</div>