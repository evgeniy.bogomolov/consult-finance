<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_cities}}`.
 */
class m190823_084716_create_user_cities_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_cities}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'cities_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_cities}}');
    }
}
