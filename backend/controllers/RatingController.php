<?php

namespace backend\controllers;

use backend\models\Rating;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class RatingController extends \yii\web\Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['manage', 'add-position'],
						'allow' => true,
						'roles' => ['manageRating'],
					],
				],
			],
		];
	}


	public function actionIndex() {
		$rating = Rating::find()->where(['not', ["user_id" => null]])->all();
		return $this->render( 'index',[
			'rating' => $rating
		]);
	}

	public function actionManage() {
		if(Yii::$app->request->post()) {
			$rating = Yii::$app->request->post('Rating');

			foreach ($rating as $position) {
				$position_item = Rating::findOne($position['id']);
				$position_item->user_id = $position['user_id'];
				$position_item->save();
			}
		}

		$rating = Rating::find()->orderBy(['id' => SORT_ASC])->all();
		$users = ArrayHelper::map(User::find()->where(['status' => User::STATUS_ACTIVE])->orderBy(["fullname" => SORT_ASC])->all(), 'id', 'fullname');
		return $this->render( 'manage', [
			'users' => $users,
			'rating' => $rating

		] );
	}
	public function actionAddPosition() {
		$position = new Rating();
		$position->save();
		$this->redirect(["rating/manage"]);
	}

}
