<?php

namespace backend\controllers;

use backend\models\Cities;
use backend\models\Departments;
use backend\models\Method;
use common\models\User;
use Yii;
use backend\models\Payments;
use backend\models\PaymentsSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentController implements the CRUD actions for Payments model.
 */
class PaymentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['viewPayments'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Payments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $filters = [];

        $cities = ArrayHelper::map(Cities::find()
            ->all(), 'id', 'city');
        $departments = ArrayHelper::map(Departments::find()
            ->all(), 'id', 'name');
        $users = ArrayHelper::map(User::find()
            ->all(), 'id', 'fullname');

        $filters['cities'] = $cities;
        $filters['departments'] = $departments;
        $filters['users'] = $users;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'filters' => $filters,
        ]);
    }

    /**
     * Lists Payments models by departments.
     * @return mixed
     */
    public function actionDepartments()
    {
        $filters = [];

        if(Yii::$app->request->get("Filters")) {

            $city_id = Yii::$app->request->get("Filters")['city_id'];

            $date_period = Yii::$app->request->get("Filters")['date'];

            $payments = Payments::paymentsByDepartmentsAndDate($city_id, $date_period);

            $filters['date'] = $date_period;
            $filters['city'] = Cities::find()->where(["id" => $city_id])->one()->city;
            $filters['city_id'] = Cities::find()->where(["id" => $city_id])->one()->id;
        } else {
            $payments = Payments::paymentsByDepartments();
        }

        $cities = ArrayHelper::map(Cities::find()
            ->all(), 'id', 'city');

        return $this->render('departments', [
            'payments' => $payments,
            'cities' => $cities,
            'filters' => $filters
        ]);
    }

    public function actionDepartmentDeals($city_id, $department_id, $date_period) {
        $date_period = explode(" - ", $date_period);
        $date_from = $date_period[0];
        $date_to = $date_period[1];

        $createdDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');
        $createdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to . ' 23:59:59');

        $payments = Payments::find()
            ->where([
                "department_id" => $department_id,
                "city_id" => $city_id
            ])
            ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
            ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null])
            ->all();

        return $this->renderPartial('department-deals', [
            'payments'=>$payments
        ]);
    }

    public function actionDepartmentDealsByEmployee($city_id, $department_id, $date_period) {

        $payments = Payments::paymentsByDepartmentsByEmployee($city_id, $department_id, $date_period);

        return $this->renderPartial('department-deals-by-employee', [
            "payments"=>$payments
        ]);
    }

    /**
     * Lists Payments models by departments.
     * @return mixed
     */
    public function actionEmployee()
    {
        $filters = [];

        if(Yii::$app->request->get("Filters")) {
            $city_id = Yii::$app->request->get("Filters")['city_id'];
            $date_period = Yii::$app->request->get("Filters")['date'];


            $payments = Payments::paymentsByEmployeeAndDate($city_id, $date_period);
            $filters['date'] = $date_period;
            $filters['city'] = Cities::find($city_id)->one()->city;
            $filters['city_id'] = Cities::find()->where(["id" => $city_id])->one()->id;
        } else {
            $payments = Payments::paymentsByEmployee();
        }

        $cities = ArrayHelper::map(\backend\models\Cities::find()
            ->all(), 'id', 'city');

        return $this->render('employee', [
            'payments' => $payments,
            'cities' => $cities,
            'filters' => $filters
        ]);
    }

    public function actionEmployeeDeals($user_id, $date_period)
    {
        $payments = Payments::paymentsByEmployeeList($user_id, $date_period);

        return $this->renderPartial('employee-deals', [
            'payments' => $payments,
        ]);
    }


    /**
     * Displays a single Payments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payments();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
