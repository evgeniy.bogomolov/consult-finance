<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\offers */

$this->title = 'Добавить сделку';
$this->params['breadcrumbs'][] = ['label' => 'Сделки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="offers-create">

    <?= $this->render('_form', [
        'model' => $model,
        'client' => $client,
        'clients' => $clients,
        'statuses' => $statuses,
        'cities' => $cities,
        'departments' => $departments,
        'users' => $users,
        'sources' => $sources
    ]) ?>

</div>
