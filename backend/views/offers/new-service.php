<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 12.06.2019
 * Time: 17:19
 */

?>
<?php
$payments = $model->payments;
$service_paid = 0;
foreach ($payments as $payment) {
    $service_paid += $payment->summ;
}
$paid_text = number_format($service_paid, 0, '', ' ');
?>
<div class="offer-service">
    <div class="row">
        <div class="col-md-3">
            <div class="offer-service-name"><?= $model->name ?></div>
        </div>
        <div class="col-md-3">
            <div class="offer-service-paid text-green"><?= $paid_text ?></div>
        </div>
        <div class="col-md-3">
            <div class="offer-service-amount"><?= number_format($model->cost, 0, '', ' '); ?></div>
        </div>
        <div class="col-md-3">
            <div class="offer-service-payment"><a href="#" class="add-payment-button">+ добавить платеж</a></div>
        </div>
    </div>

    <div class="add-payment modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Добавить платеж</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Сумма платежа</label>
                        <input type="text" name="summ" placeholder="1000" class="form-control">
                        <input type="hidden" name="service_id" value="<?= $model->id ?>">
                    </div>
                    <div class="form-group">
                        <button class="add_payment_button btn btn-success">Добавить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
