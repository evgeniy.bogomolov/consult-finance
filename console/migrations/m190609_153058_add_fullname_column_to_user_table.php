<?php

use yii\db\Migration;

/**
 * Handles adding fullname to table `{{%user}}`.
 */
class m190609_153058_add_fullname_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'fullname', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
