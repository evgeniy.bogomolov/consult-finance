<?php

use yii\db\Migration;

/**
 * Class m190612_113628_add_city_id_depatment_id_to_user_table
 */
class m190612_113628_add_city_id_depatment_id_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'city_id', $this->integer());
        $this->addColumn('{{%user}}', 'department_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190612_113628_add_city_id_depatment_id_to_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190612_113628_add_city_id_depatment_id_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
