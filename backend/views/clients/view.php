<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\clients */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clients-view">

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'phone',
        ],
    ]) ?>

    <?php if($model->offers):?>
        <h3 class="h3">Сделки клиента:</h3>
        <?php foreach ($model->offers as $offer):?>
            <div class="client__offer-id">
                <a href="<?= Url::toRoute(['offers/view', 'id' => $offer->id]); ?>">#<?= $offer->id ?> | <?= date('d.m.Y', strtotime($offer->date)) ?></a>
            </div>

        <?php endforeach; ?>
    <?php endif; ?>

    <?if($files):?>
        <h3 class="h3">Файлы:</h3>
        <div class="client__files">
            <?foreach ($files as $file):?>
                <div>
                    <?if(pathinfo($file->file, PATHINFO_EXTENSION) == 'pdf'):?>
                        <object width="100%"  type="application/pdf" data="<?= $file->file ?>?#zoom=0&scrollbar=0&toolbar=0&navpanes=0">
                            <p>Превью не поддерживается в вашем браузере</p>
                        </object>
                        <a class="fancybox" href="<?= $file->file ?>" data-fancybox-type="iframe">Открыть PDF</a>
                    <?else:?>
                        <a href="<?= $file->file ?>" alt="" class="client__file"><img src="<?= $file->file ?>" alt=""></a>
                    <?endif;?>

                </div>
            <?endforeach?>
        </div>
    <?endif;?>
    <div class="clients-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'clientFiles[]')->fileInput(['multiple' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
