<?php

use yii\db\Migration;

/**
 * Class m190706_151051_add_new_department_column_to_history
 */
class m190706_151051_add_new_department_column_to_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%history}}', 'old_department_id', $this->integer());
        $this->renameColumn('{{%history}}', 'department_id', 'new_department_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190706_151051_add_new_department_column_to_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190706_151051_add_new_department_column_to_history cannot be reverted.\n";

        return false;
    }
    */
}
