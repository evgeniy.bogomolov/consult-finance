<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title                   = 'Рейтинг';
$this->params['breadcrumbs'][] = 'Рейтинг';
?>

<?php if(Yii::$app->user->can("manageRating")):?>
<div class="rating__manage-btn">
	<?= Html::a( 'Изменить', [ 'manage' ], [ 'class' => 'btn btn-primary' ] ) ?>
</div>
<?php endif; ?>


<div class="rating">
	<?php foreach ( $rating as $position ): ?>
        <div class="position">
            <div class="position__number">
				<?= $position->id ?>
            </div>
            <div class="position__photo">
                <img src="<?= $position->user->photo ? $position->user->photo : "/images/no-photo.png"?>" alt="">
            </div>
            <div class="position__text">
                <div class="position__name">
					<?= $position->user->fullname ?>
                </div>
                <div class="position__city">
					<?php
					$i = 0;
					foreach ( $position->user->cities as $city ) {
						if ( $i != 0 ) {
							echo ", ";
						}
						echo $city->city;
						$i ++;
					}
					echo " (" . $position->user->department->name . ")";
					?>
                </div>
            </div>
        </div>
	<?php endforeach; ?>

</div>
