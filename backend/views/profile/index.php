<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('app', 'Профиль');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile">
    <p>
    <?= Html::a('Изменить', ['update'], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Сменить пароль', ['password-change'], ['class' => 'btn btn-primary']) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'email',
            'fullname',
            'phone',
            'photo',
            [
                'attribute' => 'department_id',
                'value' => function($model){
                    return $model->department->name ;
                }
            ],
            [
                'attribute' => 'city_id',
                'value' => function($model){
                    return $model->city->city ;
                }
            ]
        ],
    ]) ?>

</div>