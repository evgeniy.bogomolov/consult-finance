<?php
/* @var $this yii\web\View */
$this->title = 'Воронка продаж';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php echo $this->render('_search-statistic', [
	'filters' => $filters,
    'employee' => $employee
]);


?>

<div class="statistic">
<?php


foreach ($statistic as $department=>$users): ?>
    <h4><?= $department ?></h4>
    <?php foreach ($users as $name=>$user):?>
        <details>
            <summary><?=$name?>. Сделок: <?= $user['offers_count'] ?></summary>
            <div class="details-inner">
				<?php foreach ($user['statuses'] as $status_name => $status):?>
                    <div class="statistic__status-item">
						<?= $status_name ?> - <a class="status_offers_btn" href="/statistic/status-offers" data-offers='<?= json_encode($status['offers'],  JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK) ?>'><?= $status['count'] ?></a>
                    </div>


				<?endforeach; ?>
            </div>
        </details>
     <?endforeach;?>

<?endforeach?>

<div class="status__offers modal fade">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Сделки </h4>
            </div>
            <div class="modal-body box-body table-responsive no-padding">

            </div>
        </div>
    </div>
</div>

</div>