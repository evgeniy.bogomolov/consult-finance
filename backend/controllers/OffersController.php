<?php

namespace backend\controllers;
use backend\models\Comments;
use backend\models\Action;
use backend\models\Address;
use backend\models\Cities;
use backend\models\Clients;
use backend\models\Departments;
use backend\models\History;
use backend\models\Method;
use backend\models\OffersSearch;
use backend\models\Payments;
use backend\models\Services;
use backend\models\Source;
use backend\models\Status;
use common\models\User;
use Yii;
use backend\models\Offers;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * OffersController implements the CRUD actions for offers model.
 */
class OffersController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
	                [
		                'actions' => ['curl'],
		                'allow' => true,
		                'roles' => ['?'],
	                ],
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['Manager', 'addOffers'],
                    ],
                    [
                        'actions' => ['view', 'update', 'index'],
                        'allow' => true,
                        'roles' => ['CC', 'ED', 'SD', 'TOP', 'Manager', 'HeadOfSD', 'HeadOfED', 'HeadOfCC'],
                    ],
                    [
                        'actions' => ['add-service'],
                        'allow' => true,
                        'roles' => ['addServices'],
                    ],
                    [
                        'actions' => ['service-amount-update'],
                        'allow' => true,
                        'roles' => ['canChangeServicePrice'],
                    ],
                    [
                        'actions' => ['add-payment'],
                        'allow' => true,
                        'roles' => ['addPayments'],
                    ],
                    [
                        'actions' => ['cancel-payment'],
                        'allow' => true,
                        'roles' => ['cancelPayments'],
                    ],
                    [
                        'actions' => ['change-responsible', 'change-offer-feature', 'view-payments', 'add-action'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

	public function beforeAction($action)
	{
		if ($action->id == 'curl') {
			$this->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}


    /**
     * Lists all offers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OffersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $current_user = Yii::$app->user->getIdentity();

        //$statuses = ArrayHelper::map(\backend\models\Status::find()->all(), 'id', 'status_name');

        // получаем все отделы и их статусы
		$citiesList = ArrayHelper::map(Cities::find()
		                                     ->all(), 'id', 'city');
        $statuses = [];
        $cities = false;
        $responsibles = User::find()->all();
        if(Yii::$app->user->can("admin")) {

            $cities = Cities::find()->all();

            $statuses['КЦ'] = ArrayHelper::map(Status::find()
                ->where(["department_id"=>6])
                ->all(), 'id', 'status_name');

            $departments =  Departments::find()->all();

            foreach ($departments as $department) {
                $statuses[$department->name] = ArrayHelper::map(Status::find()
                    ->where(["department_id"=>$department->id])
                    ->all(), 'id', 'status_name');
            }
            /*foreach ($cities as $city) {
                $departments =  Departments::find()->where(["city_id"=>$city->id])->all();


                foreach ($departments as $department) {
                    $statuses[$city->city][$department->name] = ArrayHelper::map(Status::find()
                        ->where(["department_id"=>$department->id])
                        ->all(), 'id', 'status_name');
                }
            }*/
        }else {
            $responsibles = User::find()->where(["department_id" => $current_user->department_id])->all();
            $user_city_id = $current_user->city_id;

            $departments =  Departments::find()
                ->where(["id" => $current_user->department_id])
                ->all();

            foreach ($departments as $department) {
                $statuses[$department->name] = ArrayHelper::map(Status::find()
                    ->where(["department_id"=>$department->id])
                    ->all(), 'id', 'status_name');
                if($department->id == 8) { // 8 это ОИ
                    $statuses[$department->name] += ArrayHelper::map(Status::find()
                        ->where(["id"=>13]) // Статус "Передан в ОИ"
                        ->all(), 'id', 'status_name');
                    //var_dump($statuses);
                }
            }
        }

        $responsibles = ArrayHelper::map($responsibles, 'id', 'fullname');


        $clients = ArrayHelper::map(Clients::find()
            ->all(), 'id', 'name');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statuses' => $statuses,
            'clients' => $clients,
            'responsibles' => $responsibles,
	        'cities' => $citiesList
        ]);
    }

    /**
     * Displays a single offers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $comment = new Comments();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($comment->load(Yii::$app->request->post()) && $comment->comment) {
                $comment->user_id = Yii::$app->user->identity->id;
                $comment->offer_id = $model->id;

                $comment->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $clients = Clients::find()->all();
        $arClients = ArrayHelper::map($clients, 'id', 'name');


        if(Yii::$app->user->can("admin")) {
            $statuses = Status::find()
                ->all();
        } else {
            $statuses = Status::find()
                ->where(["department_id"=>Yii::$app->user->identity->department_id])
                ->all();
        }


        $arStatuses = ArrayHelper::map($statuses, 'id', 'status_name');

        if($model->status == 25) {
            $arStatuses[25] = 'Новый';
        }

        $cities = Cities::find()->all();
        $arCities = ArrayHelper::map($cities, 'id', 'city');

        $departments = Departments::find()->all();
        $arDepartments = ArrayHelper::map($departments, 'id', 'name');

        if(Yii::$app->user->can("admin")) {
            $users = User::find()->all();
        } else {
            $users = User::find()->where(["department_id" => $model->division])->all();
        }

        $arUsers = ArrayHelper::map($users, 'id', 'fullname');

        $sources = Source::find()->all();
        $arSources = ArrayHelper::map($sources, 'id', 'name');

        $addresses = Address::find()->where(["city_id"=>$model->city])->all();
        $arAddresses = ArrayHelper::map($addresses, 'name', 'name');

        $payment_methods = ArrayHelper::map(Method::find()->all(), 'id', 'name');

        $history = $model->history;

        $serviceForm = new Services();
        $actionForm = new Action();
        return $this->render('view', [
            'model' => $model,
            'departments' => $arDepartments,
            'statuses' => $arStatuses,
            'cities' => $arCities,
            'users' => $arUsers,
            'sources' => $arSources,
            'addresses' => $arAddresses,
            'payment_methods' => $payment_methods,
            'serviceForm' => $serviceForm,
            'actionForm' => $actionForm,
            'history' => $history,
            'comment' => $comment
        ]);
    }


    public function actionAddService() {
        $service = new Services();
        $service->load(Yii::$app->request->post());
        if($service->save()) {

        }

        return $this->renderPartial('new-service', [
            'model' => $service
        ]);
    }

    public function actionAddPayment($service_id, $summ, $city_id, $method_id) {
        $user_id =  Yii::$app->user->identity->id;
        $deparment_id = Yii::$app->user->identity->department_id;

        $payment = new Payments();
        $payment->service_id = $service_id;
        $payment->summ = $summ;
        $payment->user_id = $user_id;
        $payment->department_id = $deparment_id;
        $payment->city_id = $city_id;
        $payment->method_id = $method_id;

        if($payment->save()) {
            $service = Services::find()->where(['id'=>$service_id])->one();
            $payments = $service->payments;
            $service_paid = 0;
            foreach ($payments as $payment) {
                $service_paid += $payment->summ;
            }
            $paid_text = number_format($service_paid, 0, '', ' ');
        }
        else {
            $payed_text = 'Ошибка';
        }

        return $paid_text;
    }

    public function actionServiceAmountUpdate($service_id, $new_summ) {
        $service = Services::findOne($service_id);
        $service->cost = $new_summ;
        $response = "";
        if($service->save()) {
            $response = number_format($new_summ, 0, '', ' ');
        } else {
            $response = "Ошибка";
        }

        return $response;
    }

    public function actionAddAction() {
        $action = new Action();
        $action->load(Yii::$app->request->post());
        $action->datetime = date('Y-m-d H:i:s', strtotime($action->datetime));
        $action->user_id = Yii::$app->user->identity->id;

        $action->save();

        return $this->renderPartial('new-action', [
            'model' => $action
        ]);
    }

    public function actionChangeResponsible($offer_id) {
        $offer = $this->findModel($offer_id);
        $current_responsible = $offer->responsible;
        $new_responsible = Yii::$app->user->identity->id;

        if($current_responsible != $new_responsible) {
            $offer->responsible = Yii::$app->user->identity->id;

            if($offer->save()) {
                $msg = ["responsible_id"=>$new_responsible, "message"=>"Ответственный изменен"];

            } else {
                $msg = ["message"=>"Ответственный не изменен"];
            }
        } else {
            $msg = ["message"=>"Вы уже отвественный"];
        }
        $msg = json_encode($msg);
        return $msg;
    }

    public function actionChangeOfferFeature($offer_id) {
        $offer = $this->findModel($offer_id);

        if ($offer->load(Yii::$app->request->post()) && $offer->save()) {
            $msg = "Изменено";
        } else {
            $msg = "Ошибка";
        }

        return $msg;
    }

    public function actionViewPayments($service_id) {
        $payments = Services::findOne($service_id)->payments;
        return $this->renderPartial('view-payments', [
            'payments' => $payments
        ]);
    }

    public function actionCancelPayment($payment_id)
    {
        Payments::findOne($payment_id)->delete();

        return $payment_id;
    }

    public function actionCurl() {
	    $data = Yii::$app->request->post();
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if($data['token'] == 'IJhIHhUIHuIHIHPO' && $data['name'] && $data['phone'] && $data['summ'] && $data['city']) {
	    	$client = Clients::find()->where(['phone' => $data['phone']])->one();

			if($client) {
				$offer = Offers::find()->where(['client_id' => $client->id])->one();
				$offer->status = '25';
				$offer->date = date('Y-m-d H:i:s');
			} else {
				$client = new Clients();
				$client->name = $data['name'];
				$client->phone = $data['phone'];
				$client->save();

				$offer = new Offers();
				$offer->source = 1;
				$offer->credit_summ = $data['summ'];
				$offer->city = $data['city'];
				$offer->client_id = $client->id;
			}
		    $offer->save();

	    } else {
	    	echo "Bad request";
	    }

	    return $data;
    }

    /**
     * Creates a new offers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new offers();
        $client = new Clients();


        if ($client->load(Yii::$app->request->post())) {
	        $client->phone = preg_replace('/[^0-9]/', '', $client->phone);
            if(!$client->save() && array_key_exists('phone', $client->getErrors())) {
                $client = Clients::find()->where(["phone" => $client->phone])->one();
                $flash = "Клиент $client->name был добавлен ранее. ";
            } else {
                $flash = "Клиент $client->name добавлен. ";
            }

            if ($model->load(Yii::$app->request->post())) {
                $model->client_id = $client->id;
                if($model->save()) {
                    $flash .= "Сделка создана.";
                    Yii::$app->session->setFlash('success', $flash);
                }
            }


            if(Yii::$app->user->can('Manager')) {
                Yii::$app->session->setFlash('success', 'Сделка успешно добавлена');
                return $this->goHome();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $clients = Clients::find()->all();
        $arClients = ArrayHelper::map($clients, 'id', 'name');


        $statuses = Status::find()->all();
        $arStatuses = ArrayHelper::map($statuses, 'id', 'status_name');

        $cities = Cities::find()->all();
        $arCities = ArrayHelper::map($cities, 'id', 'city');

        $departments = Departments::find()->all();
        $arDepartments = ArrayHelper::map($departments, 'id', 'name');

        $users = User::find()->all();
        $arUsers = ArrayHelper::map($users, 'id', 'fullname');

        $sources = Source::find()->all();
        $arSources = ArrayHelper::map($sources, 'id', 'name');

        return $this->render('create', [
            'model' => $model,
            'client' => $client,
            'clients' => $arClients,
            'statuses' => $arStatuses,
            'cities' => $arCities,
            'departments' => $arDepartments,
            'users' => $arUsers,
            'sources' => $arSources
        ]);
    }

    /**
     * Updates an existing offers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $clients = Clients::find()->all();
        $arClients = ArrayHelper::map($clients, 'id', 'name');


        $statuses = Status::find()->all();
        $arStatuses = ArrayHelper::map($statuses, 'id', 'status_name');

        $cities = Cities::find()->all();
        $arCities = ArrayHelper::map($cities, 'id', 'city');

        $departments = Departments::find()->all();
        $arDepartments = ArrayHelper::map($departments, 'id', 'name');

        $users = User::find()->all();
        $arUsers = ArrayHelper::map($users, 'id', 'fullname');

        $sources = Source::find()->all();
        $sources = Source::find()->all();
        $arSources = ArrayHelper::map($sources, 'id', 'name');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'clients' => $arClients,
            'statuses' => $arStatuses,
            'cities' => $arCities,
            'departments' => $arDepartments,
            'users' => $arUsers,
            'sources' => $arSources
        ]);
    }

    /**
     * Deletes an existing offers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMassDelete() {
    	//сделки, услуги, платежи, контакты, историю 2019-11-09 00:00:00
    	$offers = Offers::find()->where(["division" => 6])
	                            ->andFilterWhere(['<=', 'date', '2019-11-12 00:00:00']);
    	$offer_ids = $offers->select("id")->asArray()->column();

    	$services = Services::find()->where(["offer_id" => $offer_ids]);
	    $services_ids = $services->select("id")->asArray()->column();

    	$payments = Payments::find()->where(['service_id' => $services_ids]);

	    $history = History::find()->where(["offer_id" => $offer_ids]);

	    $deleter_history = History::deleteAll(["offer_id" => $offer_ids]);
	    echo "Записей в историй удалено ".$deleter_history."<br>";

	    $deleted_payments = Payments::deleteAll(['service_id' => $services_ids]);
	    echo "Платежей удалено ".$deleted_payments."<br>";

	    $deleted_services = Services::deleteAll(["offer_id" => $offer_ids]);
	    echo "Услуг удалено ".$deleted_services."<br>";

	    $deleted_offers = Offers::deleteAll(["id" => $offer_ids]);
	    echo "Сделок удалено ".$deleted_offers."<br>";

	    return $this->render('mass-delete');
    }

    /**
     * Finds the offers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return offers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = offers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
