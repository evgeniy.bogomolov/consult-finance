<?php

use yii\db\Migration;

/**
 * Class m190917_142358_add_files_column_to_clients
 */
class m190917_142358_add_files_column_to_clients extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%clients}}', 'files', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190917_142358_add_files_column_to_clients cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190917_142358_add_files_column_to_clients cannot be reverted.\n";

        return false;
    }
    */
}
