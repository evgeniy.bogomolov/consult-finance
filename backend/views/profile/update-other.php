<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 15.08.2019
 * Time: 16:19
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

$this->title = "Редактирование профиля ".$model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['site/users']];
$this->params['breadcrumbs'][] = $this->title;

?>
<p>
    <?php if($model->status == User::STATUS_ACTIVE): ?>
        <?= Html::a('Заблокировать пользователя', ['block', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    <?php endif; ?>

    <?php if($model->status == User::STATUS_INACTIVE): ?>
        <?= Html::a('Разблокировать пользователя', ['unblock', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    <?php endif; ?>

    <?= Html::a('Изменить пароль', ['password-change-other', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
</p>
<p>
<?php $form = ActiveForm::begin([]); ?>

    <?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>
    <?= $form->field($model, 'fullname')->input('text') ?>
    <?= $form->field($model, 'birthday')->textInput() ?>
    <?= $form->field($model, 'phone')->textInput() ?>
    <?= $form->field($model, 'role')->dropDownList($roles, [
                "prompt" => "---"
            ]

    ) ?>
    <?= $form->field($model, 'department_id')->dropDownList($departments, [
        "prompt" => "---"]) ?>
    <?=
    $form->field($model, 'cities_ids')->checkboxList($cities);
    ?>
	<?= $form->field($model, 'user_photo')->fileInput() ?>
    <?php if($model->photo): ?>
        <img src="<?= $model->photo?> " width="280px" alt="" style="margin-bottom: 30px">
	    <?= Html::a('Удалить фото', ['delete-photo', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    <?endif;?>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
</p>
