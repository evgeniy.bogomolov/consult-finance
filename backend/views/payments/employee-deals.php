<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 17.06.2019
 * Time: 19:07
 */
?>


<div class="cash-wrap box-body table-responsive no-padding"></div>
<table class="table table-bordered">
    <thead class="thead">
    <tr>
        <td>
            Дата
        </td>
        <td>
            Сумма
        </td>
        <td>
            Способ оплаты
        </td>
        <td>
            Контакт
        </td>
        <td>
            Услуга
        </td>
    </tr>
    </thead>
    <tbody>
    <?foreach ($payments as $payment):?>
        <tr>
            <td>
                <?= date('d.m.Y H:m', strtotime($payment->date))?>
            </td>
            <td>
                <?= $payment["summ"] ?>
            </td>
            <td>
                <?= $payment["method"] ?>
            </td>
            <td>
                <?= $payment["client"] ?>
            </td>
            <td>
                <?= $payment["service"] ?>
            </td>
        </tr>
    <?endforeach;?>
    </tbody>
</table>