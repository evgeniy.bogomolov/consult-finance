<?php

use yii\db\Migration;

/**
 * Class m190917_134929_add_color_column_to_status
 */
class m190917_134929_add_color_column_to_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%status}}', 'color', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190917_134929_add_color_column_to_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190917_134929_add_color_column_to_status cannot be reverted.\n";

        return false;
    }
    */
}
