<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статусы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-index">

    <p>
        <?= Html::a('Добавить статус', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'status_name',
            [
                'attribute' => 'department_id',
                'value' => function($model) {
                    return $model->department->name;
                }
            ],
            [
                'attribute' => 'color',
                'format' => 'html',
                'value' => function($model) {
                    $color = "<div class='status__color' style='background: $model->color'></div>";
                    return $color;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
