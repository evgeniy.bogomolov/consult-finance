<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_cities".
 *
 * @property int $id
 * @property int $user_id
 * @property int $cities_id
 */
class UserCities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'cities_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'cities_id' => 'Cities ID',
        ];
    }
}
