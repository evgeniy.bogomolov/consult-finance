<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Method */
?>
<div class="method-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
