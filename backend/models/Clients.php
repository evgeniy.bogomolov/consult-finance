<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\web\UploadedFile;
use backend\models\ClientsFiles;

/**
 * This is the model class for table "Clients".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property array $services
 * @property array $clients
 */
class Clients extends \yii\db\ActiveRecord
{

    public $clientFiles;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['files'], 'string'],
            [['phone'], 'unique'],
            [['clientFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, pdf', 'maxFiles' => 4]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'clientFiles' => 'Файлы'
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->phone = preg_replace('/[^0-9]/', '', $this->phone);

            return true;
        }
        return false;
    }

    function random_string ($str_length)
    {
        $str_characters = array (0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');


        if (!is_int($str_length) || $str_length < 0)
        {
            return false;
        }

        // Подсчитываем реальное количество символов, участвующих в формировании случайной строки и вычитаем 1
        $characters_length = count($str_characters) - 1;

        // Объявляем переменную для хранения итогового результата
        $string = '';

        // Формируем случайную строку в цикле
        for ($i = $str_length; $i > 0; $i--)
        {
            $string .= $str_characters[mt_rand(0, $characters_length)];
        }

        // Возвращаем результат
        return $string;
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->clientFiles as $clientFile) {
                $random_name = $this->random_string(15);
                $file_name = 'uploads/' . $random_name . '.' . $clientFile->extension;

	            $clientFile->saveAs(Yii::getAlias('@backend')."/web/".$file_name);
                $file = new ClientsFiles();
                $file->file = "/".$file_name;
                $file->client_id = $this->id;
                $file->save();
            }
            return true;
        } else {
            return false;
        }
        return true;
    }

    public function getOffers()
    {
        return $this->hasMany(Offers::className(), ['client_id' => 'id'])->orderBy(['date'=>SORT_DESC]);
    }

    public function getFiles()
    {
        return $this->hasMany(ClientsFiles::className(), ['client_id' => 'id']);
    }

    public function getNamephone()
    {
        return $this->name . ' ' . $this->phone;
    }
}