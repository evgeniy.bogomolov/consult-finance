<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 09.06.2019
 * Time: 19:52
 */
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = "Пользователи";
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'username',
        [
            'attribute' => 'fullname',
            'label' => 'ФИО',
            'format' => 'html',
            'value' => function($model){
                if($model->status == 9) {
                    return "$model->fullname <span class='not-set'>(заблокирован)</span>";
                } else {
                    return $model->fullname;
                }
            }
        ],
        [
            'attribute' => 'edit',
            'label' => 'Действие',
            'format' => 'html',
            'value' => function($model){
                return Html::a('Изменить', ['profile/update-other', "id" => $model->id]);
            }
        ]
    ],
]); ?>
