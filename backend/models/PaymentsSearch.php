<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Payments;

/**
 * PaymentsSearch represents the model behind the search form of `backend\models\Payments`.
 */
class PaymentsSearch extends Payments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'summ', 'user_id', 'department_id', 'service_id', 'city_id'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> [
                'defaultOrder' => [
                    'date' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $date_period = explode(" - ", $this->date);
        $date_from = $date_period[0];
        $date_to = $date_period[1];

        $createdDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');

        $createdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to . ' 23:59:59');

        $query
            ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
            ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'summ' => $this->summ,
            'user_id' => $this->user_id,
            'department_id' => $this->department_id,
            'service_id' => $this->service_id,
            'city_id' => $this->city_id,
        ]);

        return $dataProvider;
    }
}
