<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
/**
 * Created by PhpStorm.
 * User: g
 * Date: 15.06.2019
 * Time: 14:31
 */
class RbacController extends Controller
{
    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        $admin = $auth->createRole('admin');
        $CC = $auth->createRole('CC');
        $SD = $auth->createRole('SD');
        $ED = $auth->createRole('ED');

        $auth->add($admin);
        $auth->add($CC);
        $auth->add($SD);
        $auth->add($ED);

        $auth->addChild($SD, $CC);
        $auth->addChild($ED, $SD);
        $auth->addChild($admin, $ED);

        $auth->assign($admin, 1);

        $auth->assign($CC, 3);
    }
}