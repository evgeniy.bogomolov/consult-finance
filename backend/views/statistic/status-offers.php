<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 02.11.2019
 * Time: 19:56
 */
?>

<table class="table table-bordered">
	<thead class="thead">
	<tr>
		<td>
			ID
		</td>
		<td>
			Статус
		</td>
		<td>
			Последнее изменение
		</td>
		<td>
			Клиент
		</td>
		<td>
			Сотрудник
		</td>
	</tr>
	</thead>
	<tbody>
	<?php foreach($offers as $offer): ?>
	<tr>
		<td>
            <a href="<?=Yii::$app->urlManager->createUrl(['offers/view', 'id' => $offer->id])?>"><?= $offer->id ?></a>
		</td>
		<td>
			<?= $offer->statusname->status_name ?>
		</td>
		<td>
            <?= $offer->updated_at ? date('d.m.Y',strtotime($offer->updated_at)) : "Не задано"?>
		</td>
        <td>
            <?= $offer->client->name ?>
        </td>
		<td>
			<?= $offer->responsibleName->fullname ?>
		</td>
	</tr>
	<?endforeach;?>
	</tbody>
</table>