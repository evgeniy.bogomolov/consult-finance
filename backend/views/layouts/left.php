<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
	                ['label' => 'Рейтинг', 'icon' => 'star', 'url' => ['/rating/index']],
                    ['label' => 'Клиенты', 'icon' => 'users', 'url' => ['/clients/index'], 'visible' => Yii::$app->user->can('addContact')],
                    ['label' => 'Сделки', 'icon' => 'th-list', 'url' => ['/offers/index']],
                    ['label' => 'Новая сделка', 'icon' => 'plus', 'url' => ['/offers/create'], 'visible' => Yii::$app->user->can('addOffers')],

                    [
                        "label" => "Настройки",
                        "icon" => "th",
                        "url" => "#",
                        'visible' => Yii::$app->user->can('settings'),
                        "items" => [
                            ['label' => 'Города', 'url' => ['/cities/index']],
                            ['label' => 'Отделы', 'url' => ['/departments/index']],
                            ['label' => 'Адреса', 'url' => ['/address/index']],
                            ['label' => 'Источники', 'url' => ['/source/index']],
                            ['label' => 'Статусы', 'url' => ['/status/index']],
                            ['label' => 'Способы оплаты', 'url' => ['/methods/index']],
                            ['label' => 'Добавить пользователя', 'url' => ['/site/signup']],
                            ['label' => 'Пользователи', 'url' => ['/site/users']],
                            ['label' => 'Удалить город', 'url' => ['/cities/delete-city/']],
                            ["label" => "Менеджер доступов",
                                "icon" => "th",
                                "url" => "#",
                                "items" => [
                                    ['label' => 'Разрешения', 'url' => ['/rbac/rule']],
                                    ['label' => 'Права', 'url' => ['/rbac/permission']],
                                    ['label' => 'Роли', 'url' => ['/rbac/role']],
                                    ['label' => 'Связи', 'url' => ['/rbac/assignment']],
                                ],
                            ],
                        ],
                    ],

                    [
                        'label' => 'Касса',
                        'icon' => 'dollar',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('viewPayments'),
                        "items" => [
                            ['label' => 'Все платежи', 'url' => ['/payments/index']],
                            ['label' => 'По отделам', 'url' => ['/payments/departments']],
                            ['label' => 'По сотрудникам', 'url' => ['/payments/employee']],
                            ['label' => 'Воронка', 'url' => ['/statistic/index']],
                        ]
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
