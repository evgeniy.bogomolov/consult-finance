<?php

namespace common\models;

use backend\models\Cities;
use backend\models\Departments;
use backend\models\UserCities;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $fullname
 * @property string $phone
 * @property string $birthday
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $department_id
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface {
	const STATUS_DELETED = 0;
	const STATUS_INACTIVE = 9;
	const STATUS_ACTIVE = 10;

	public $role;
	public $cities_ids;
	public $user_photo;


	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%user}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

	public function saveOther() {
		if ( ! $this->validate() ) {
			return null;
		}

		UserCities::deleteAll( [ 'user_id' => $this->id ] );
		foreach ( $this->cities_ids as $user_city ) {
			$user_cities            = new UserCities();
			$user_cities->user_id   = $this->id;
			$user_cities->cities_id = $user_city;
			$user_cities->save();
		}

		$current_role = array_keys( Yii::$app->authManager->getRolesByUser( $this->id ) )[0];
		$manager      = Yii::$app->authManager;
		$item         = $manager->getRole( $current_role );
		$item         = $item ?: $manager->getPermission( $current_role );
		$manager->revoke( $item, $this->id );

		$new_role = $manager->getRole( $this->role );
		$manager->assign( $new_role, $this->id );

		$saved = $this->save();

		return $saved;

	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ 'status', 'default', 'value' => self::STATUS_ACTIVE ],
			[ 'status', 'in', 'range' => [ self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED ] ],
			[ [ 'fullname', 'birthday', 'phone' ], 'string' ],
			[ [ 'department_id' ], 'integer' ],
			[ [ 'email' ], 'string' ],
			[ [ 'role' ], 'string' ],
			[ [ 'cities_ids' ], 'safe' ],
			[['photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, pdf'],
			[['user_photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, pdf'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'username'      => 'Логин',
			'fullname'      => 'ФИО',
			'phone'         => 'Телефон',
			'birthday'      => 'Дата рождения',
			'department_id' => 'Отдел',
			'city_id'       => 'Город',
			'cities_ids'    => 'Города',
			'photo' => 'Фотография',
			'user_photo' => 'Фотография',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public static function findIdentity( $id ) {
		return static::findOne( [ 'id' => $id, 'status' => self::STATUS_ACTIVE ] );
	}

	/**
	 * {@inheritdoc}
	 */
	public static function findIdentityByAccessToken( $token, $type = null ) {
		throw new NotSupportedException( '"findIdentityByAccessToken" is not implemented.' );
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 *
	 * @return static|null
	 */
	public static function findByUsername( $username ) {
		return static::findOne( [ 'username' => $username, 'status' => self::STATUS_ACTIVE ] );
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 *
	 * @return static|null
	 */
	public static function findByPasswordResetToken( $token ) {
		if ( ! static::isPasswordResetTokenValid( $token ) ) {
			return null;
		}

		return static::findOne( [
			'password_reset_token' => $token,
			'status'               => self::STATUS_ACTIVE,
		] );
	}

	/**
	 * Finds user by verification email token
	 *
	 * @param string $token verify email token
	 *
	 * @return static|null
	 */
	public static function findByVerificationToken( $token ) {
		return static::findOne( [
			'verification_token' => $token,
			'status'             => self::STATUS_INACTIVE
		] );
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 *
	 * @return bool
	 */
	public static function isPasswordResetTokenValid( $token ) {
		if ( empty( $token ) ) {
			return false;
		}

		$timestamp = (int) substr( $token, strrpos( $token, '_' ) + 1 );
		$expire    = Yii::$app->params['user.passwordResetTokenExpire'];

		return $timestamp + $expire >= time();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getId() {
		return $this->getPrimaryKey();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAuthKey() {
		return $this->auth_key;
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateAuthKey( $authKey ) {
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 *
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword( $password ) {
		return Yii::$app->security->validatePassword( $password, $this->password_hash );
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword( $password ) {
		$this->password_hash = Yii::$app->security->generatePasswordHash( $password );
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey() {
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken() {
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	public function generateEmailVerificationToken() {
		$this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken() {
		$this->password_reset_token = null;
	}

	public function getDepartment() {
		return $this->hasOne( Departments::className(), [ 'id' => 'department_id' ] );
	}

	public function getCity() {
		return $this->hasOne( Cities::className(), [ 'id' => 'city_id' ] );
	}

	public function getCities() {
		return $this->hasMany( Cities::className(), [ 'id' => 'cities_id' ] )
		            ->viaTable( 'user_cities', [ 'user_id' => 'id' ] );
	}

	function random_string( $str_length ) {
		$str_characters = array(
			0,
			1,
			2,
			3,
			4,
			5,
			6,
			7,
			8,
			9,
			'a',
			'b',
			'c',
			'd',
			'e',
			'f',
			'g',
			'h',
			'i',
			'j',
			'k',
			'l',
			'm',
			'n',
			'o',
			'p',
			'q',
			'r',
			's',
			't',
			'u',
			'v',
			'w',
			'x',
			'y',
			'z',
			'A',
			'B',
			'C',
			'D',
			'E',
			'F',
			'G',
			'H',
			'I',
			'J',
			'K',
			'L',
			'M',
			'N',
			'O',
			'P',
			'Q',
			'R',
			'S',
			'T',
			'U',
			'V',
			'W',
			'X',
			'Y',
			'Z'
		);


		if ( ! is_int( $str_length ) || $str_length < 0 ) {
			return false;
		}

		// Подсчитываем реальное количество символов, участвующих в формировании случайной строки и вычитаем 1
		$characters_length = count( $str_characters ) - 1;

		// Объявляем переменную для хранения итогового результата
		$string = '';

		// Формируем случайную строку в цикле
		for ( $i = $str_length; $i > 0; $i -- ) {
			$string .= $str_characters[ mt_rand( 0, $characters_length ) ];
		}

		// Возвращаем результат
		return $string;
	}

	public function upload() {

		if ( $this->validate() ) {
			$random_name = $this->random_string( 15 );
			$file_name   = 'uploads/' . $random_name . '.' . $this->user_photo->extension;
			$this->user_photo->saveAs( $file_name );
			$this->photo = "/".$file_name;
			$this->user_photo = false;
			return true;
		} else {
			return false;
		}
	}

}
