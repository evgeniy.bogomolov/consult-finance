<?php

use yii\db\Migration;

/**
 * Handles adding status to table `{{%offers}}`.
 */
class m190411_135656_add_status_column_to_offers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%offers}}', 'status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%offers}}', 'status');
    }
}
