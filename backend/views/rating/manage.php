<?php
/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->title                   = 'Управление рейтингом';
$this->params['breadcrumbs'][] = ['label' => 'Рейтинг', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Управление рейтингом';
?>
<div class="rating__add-pos-btn">
<?= Html::a('Добавить позицию', ['add-position'], ['class' => 'btn btn-success']) ?>
</div>
<?php $form = ActiveForm::begin([]); ?>
<?php foreach ($rating as $position ):?>
    <div class="rating__item">
        <div class="rating__position">
	        <?= $position->id ?>
        </div>
        <div class="rating__user">
	        <?= Html::hiddenInput("Rating[$position->id][id]", $position->id) ?>
	        <?= Html::dropDownList("Rating[$position->id][user_id]",$position->user_id, $users,["class"=>"form-control", "prompt" => "---"] ) ?>

        </div>
    </div>
<?php endforeach;?>
<div class="form-group">
	<?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
