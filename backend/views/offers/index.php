<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Сделки';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="offers-index">
	<?php echo $this->render( '_search', [
		'model'        => $searchModel,
		'statuses'     => $statuses,
		'clients'      => $clients,
		'responsibles' => $responsibles,
		'cities'       => $cities
	] ); ?>
    <div class="offers-list__export">
		<?php
		if ( Yii::$app->user->can( "makeExport" ) ) {
			echo ExportMenu::widget( [
				'dataProvider' => $dataProvider,
				'columns'      => [
					'id',
					[
						'attribute' => 'client_id',
						'label'     => 'Контакт',
						'value'     => function ( $model ) {
							return $model->client->name;
						},
					],
					[
						'label' => 'Телефон',
						'value' => function ( $model ) {
							return $model->client->phone;
						},
					],
				]
			] );
		}

		?>
    </div>
    <div class="offers-list box-body table-responsive no-padding">


		<?= GridView::widget( [
			'dataProvider' => $dataProvider,
			'columns'      => [
				'id',
				[
					'class'   => 'yii\grid\ActionColumn',
					'buttons' => [
						'update' => function ( $url, $model, $key ) {
							return false;
						},
						'delete' => function ( $url, $model, $key ) {
							return false;
						},
					],

				],
				[
					'attribute' => 'status',
					//'filter' => $statuses,
					//'filterInputOptions' => ['class' => 'form-control form-control-sm'],
					'format'    => 'html',
					'value'     => function ( $model ) {
						return "<div class='offer__status'><span class='status__color' style='background:" . $model->statusname->color . "'></span><span>" . $model->statusname->status_name . "</span></div>";
					},
				],
				[
					'attribute' => 'division',
					'label'     => 'Отдел',
					'value'     => function ( $model ) {

						return $model->department->name;
					},
				],
				[
					'attribute' => 'action',
					'label'     => 'Задача',
					'value'     => function ( $model ) {
						$action = $model->actions[0];
						if ( ! $action ) {
							return "Задач нет";
						}
						$action_name = $action->name == "meeting" ? "Назначена встреча" : "Назначен звонок";
						$action_date = date( 'd.m.Y H:i', strtotime( $action->datetime ) );
						$str         = $action_name . " " . $action_date;

						return $str;
					},
				],
				[
					'attribute' => 'date',
					'value'     => function ( $model ) {
						return date( 'd.m.Y', strtotime( $model->date ) );
					},
				],
				[
					'attribute' => 'client_id',
					'label'     => 'Контакт',
					'value'     => function ( $model ) {
						return $model->client->name;
					},
				],
				[
					'attribute' => 'responsible',
					'format'    => 'html',
					'value'     => function ( $model ) {
						if ( $model->responsibleName ) {
							$responsible = $model->responsibleName->fullname . "(" . $model->responsibleName->department->name . ")";
						} else {
							$responsible = "<span class='not-set'>Не назначен</span>";
						}

						return $responsible;
					},
				],
				[
					'attribute' => 'services',
					'label'     => 'Услуги',
					'format'    => 'html',
					'value'     => function ( $model ) {
						$services = $model->services;
						$str      = "";
						foreach ( $services as $service ) {
							$service_name = $service->name;
							$payments     = $service->payments;
							$service_paid = 0;
							foreach ( $payments as $payment ) {
								$service_paid += $payment->summ;
							}
							$service_cost = $service->cost;
							$str          .= $service_name . " (<span class='text-green'>" . $service_paid . "</span>/" . $service_cost . ")<br>";
						}

						return $str;
					},
				],
				//'responsible',
				//'source',
			],
		] ); ?>
    </div>

</div>

