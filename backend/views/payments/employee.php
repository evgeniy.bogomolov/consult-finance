<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Касса';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
echo $this->render('_search-employee', [
    'cities' => $cities,
    'filters' => $filters
]);
?>

<div class="cash">
    <div class="cash-wrap box-body table-responsive no-padding">
    <table class="table emp-table table-bordered">
        <thead class="thead">
        <?if(Yii::$app->request->get("Filters")):?>
            <tr>
                <td colspan="6">
                    <?= $filters['date'] ?>, <?= $filters['city'] ?>

                </td>
            </tr>
        <?endif;?>
        <tr>
            <td>
                Сотрудник
            </td>
            <td>
                Отдел
            </td>
            <td>
                Сумма
            </td>
            <td>
                Количество сделок
            </td>
            <td>
                Способы оплаты
            </td>
            <td>
                Действия
            </td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($payments as $payment): ?>
        <tr>
            <td>
                <?= $payment['user_name'] ?>
            </td>
            <td>
                <?= $payment['department'] ?>
            </td>
            <td>
                <?= $payment['summ'] ?>
            </td>
            <td>
                <?= $payment['count'] ?>
            </td>
            <td>
                <?php foreach ($payment['methods'] as $method_name => $method_summ){ ?>
                    <?= $method_name ?>: <?= number_format($method_summ, 0, '', ' ')?> <br>
                <?php } ?>
            </td>
            <td>
                <a href="/payments/employee-deals?user_id=<?= $payment['user_id'] ?>&date_period=<?= $filters['date'] ?>" class="show-employee-payments" data-toggle="modal" data-target="#emp-deals">Показать сделки</a>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>

<div class="emp-deals modal fade">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Сделки сотрудника</h4>
            </div>
            <div class="modal-body box-body table-responsive no-padding">

            </div>
        </div>
    </div>
</div>