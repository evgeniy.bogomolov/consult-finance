<?php use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Редактирование профиля');
$this->params['breadcrumbs'][] = ['label' => 'Профиль', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([]); ?>

<?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>
<?= $form->field($model, 'fullname')->input('text') ?>
<?= $form->field($model, 'birthday')->textInput() ?>
<?= $form->field($model, 'phone')->textInput() ?>
<?= $form->field($model, 'user_photo')->fileInput() ?>
<?php if($model->photo): ?>
    <img src="<?= $model->photo?> " width="280px" alt="" style="margin-bottom: 30px">
	<?= Html::a('Удалить фото', ['delete-photo', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
<?endif;?>
<?php if(Yii::$app->user->can("admin")): ?>
    <?= $form->field($model, 'department_id')->dropDownList($departments, ["prompt" => "---"]) ?>
    <?= $form->field($model, 'city_id')->dropDownList($cities, ["prompt" => "---"]) ?>
<?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>