<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 06.07.2020
 * Time: 15:37
 */
use yii\bootstrap\ActiveForm;
$this->title = "Удалить город";
?>


<? $form = ActiveForm::begin([]); ?>
<div class="form-group">
	<?= \yii\bootstrap\Html::dropDownList(
	        'city',
            Yii::$app->request->post('city'),
            \yii\helpers\ArrayHelper::map($cities, 'id', 'city')) ?>
</div>

<div class="form-group">
	<?= \yii\bootstrap\Html::submitButton('Выбрать', ['class' => 'btn-info btn']) ?>
</div>


<?php ActiveForm::end(); ?>
<br>
<hr>
<br>

<? if($data):?>
<p>

<?php foreach ($data as $key => $value):?>
	<strong><?= $key ?>:</strong> <?= $value ?> <br>
<?endforeach?>
</p>
<p>
	<? $form = ActiveForm::begin([
	        'id' => 'delete-form'
    ]); ?>
    <?= \yii\helpers\Html::hiddenInput('delete', 1) ?>
    <?= \yii\helpers\Html::hiddenInput('city', Yii::$app->request->post('city')) ?>
	    <?= \yii\bootstrap\Html::submitButton('Удалить', ['class' => 'btn-danger btn']) ?>
	<?php ActiveForm::end(); ?>
</p>


<?endif?>