<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property int $status
 */
class AddOffer extends \yii\db\ActiveRecord
{


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'city', 'division', 'responsible', 'source'], 'integer'],
            [['date'], 'safe'],
            [['status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

        ];
    }
}
