<?php

namespace backend\controllers;

use backend\models\Departments;
use backend\models\History;
use backend\models\Offers;
use backend\models\Status;
use common\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use Yii;

class StatisticController extends \yii\web\Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['viewFunnel'],
					],
				],
			],
		];
	}

    public function actionIndex()
    {

		$statistic = [];
	    $filters = [];
	    $employee = ArrayHelper::map(User::find()->all(), 'id', 'fullname');

	    $departments = Departments::find()->all();

	    if(Yii::$app->request->get("Filters")) {

		    $date_period = Yii::$app->request->get("Filters")['date'];
		    $employee_id = Yii::$app->request->get("Filters")['employee'];
		    $filters['date'] = $date_period;
		    $filters['employee'] = $employee_id;

		    $date_period = explode(" - ", $date_period);
		    $date_from = $date_period[0];
		    $date_to = $date_period[1];

		    $createdDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');

		    $createdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to . ' 23:59:59');

		    foreach ( $departments as $department ) {
			    if($filters['employee']) {
				    $users = User::find()->where(["department_id" => $department->id, "id" => $employee_id])->all();
			    } else {
				    $users = User::find()->where(["department_id" => $department->id])->all();
			    }
			    foreach ($users as $user) {
				    $offers_count = History::find()
				                           ->where(["new_responsible_id" => $user->id])
				                           ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
				                           ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null])
				                           ->count();

				    $statistic[$department->name][$user->fullname]["offers_count"] = $offers_count;
				    $statuses = Status::find()->where(["department_id" => $department->id])->all();
				    foreach ($statuses as $status) {

					    $status_offers = History::find()
					                            ->where(["user_id" => $user->id, "new_status_id" => $status->id])
					                            ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
					                            ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null]);

					    $offers = $status_offers->select('id')->asArray()->column();


					    $statistic[$department->name][$user->fullname]["statuses"][$status->status_name]["count"] = $status_offers->count();
					    $statistic[$department->name][$user->fullname]["statuses"][$status->status_name]["offers"] = $offers;
				    }

			    }
		    }

	    } else {

		    foreach ( $departments as $department ) {
			    $users = User::find()->where(["department_id" => $department->id])->all();

			    foreach ($users as $user) {
				    $offers_count = History::find()->where(["new_responsible_id" => $user->id])->count();
				    $statistic[$department->name][$user->fullname]["offers_count"] = $offers_count;

				    $statuses = Status::find()->where(["department_id" => $department->id])->all();
				    foreach ($statuses as $status) {

					    $status_offers = History::find()->where(["user_id" => $user->id, "new_status_id" => $status->id]);
					    $offers = $status_offers->select('offer_id')->asArray()->column();

					    $statistic[$department->name][$user->fullname]["statuses"][$status->status_name]["count"] = $status_offers->count();
					    $statistic[$department->name][$user->fullname]["statuses"][$status->status_name]["offers"] = $offers;
				    }

			    }
		    }

	    }



        return $this->render('index',[
        	"users" => $users,
        	"statistic" => $statistic,
	        'filters' => $filters,
	        'employee' => $employee
        ]);
    }

	public function actionStatusOffers() {
    	$offers_ids = Yii::$app->request->get();

    	$offers = Offers::find()->where(["IN","id", $offers_ids['offers_ids']])->all();
		return $this->renderPartial('status-offers', [
			'offers' => $offers
		]);
	}

}
