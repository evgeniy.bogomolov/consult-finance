<?php

use yii\db\Migration;

/**
 * Class m190611_164810_add_offer_id_column_to_services
 */
class m190611_164810_add_offer_id_column_to_services extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%services}}', 'offer_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190611_164810_add_offer_id_column_to_services cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190611_164810_add_offer_id_column_to_services cannot be reverted.\n";

        return false;
    }
    */
}
