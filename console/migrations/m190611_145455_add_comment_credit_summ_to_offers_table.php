<?php

use yii\db\Migration;

/**
 * Class m190611_145455_add_comment_credit_summ_to_offers_table
 */
class m190611_145455_add_comment_credit_summ_to_offers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%offers}}', 'credit_summ', $this->string());
        $this->addColumn('{{%offers}}', 'comment', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190611_145455_add_comment_credit_summ_to_offers_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190611_145455_add_comment_credit_summ_to_offers_table cannot be reverted.\n";

        return false;
    }
    */
}
