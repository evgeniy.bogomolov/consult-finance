<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 17.06.2019
 * Time: 19:07
 */
?>
<div class="cash-wrap box-body table-responsive no-padding">
<table class="table table-bordered">
    <thead class="thead">
    <tr>
        <td>
            Дата
        </td>
        <td>
            Сумма
        </td>
        <td>
            Контакт
        </td>
        <td>
            Услуга
        </td>
        <td>
            Сотрудник
        </td>
    </tr>
    </thead>
    <tbody>
    <?foreach ($payments as $payment):?>
    <tr>
        <td>
            <?= date('d.m.Y H:m', strtotime($payment->date))?>
        </td>
        <td>
            <?= $payment->summ ?>
        </td>
        <td>
            <?= $payment->service->offer->client->name ?>
        </td>
        <td>
            <?= $payment->service->name ?>
        </td>
        <td>
            <?= $payment->responsible->fullname ?>
        </td>
    </tr>
    <?endforeach;?>
    </tbody>
</table>
</div>