<?php

use yii\db\Migration;

/**
 * Class m190919_134835_add_client_id_to_clientsFiles
 */
class m190919_134835_add_client_id_to_clientsFiles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%clientsFiles}}', 'client_id', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190919_134835_add_client_id_to_clientsFiles cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190919_134835_add_client_id_to_clientsFiles cannot be reverted.\n";

        return false;
    }
    */
}
