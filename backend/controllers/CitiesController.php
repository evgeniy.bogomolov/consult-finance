<?php

namespace backend\controllers;

use backend\models\Action;
use backend\models\Address;
use backend\models\Comments;
use backend\models\History;
use backend\models\Offers;
use backend\models\Payments;
use backend\models\Services;
use Yii;
use backend\models\Cities;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CitiesController implements the CRUD actions for cities model.
 */
class CitiesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all cities models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => cities::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single cities model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new cities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new cities();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing cities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing cities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	public function actionDeleteCity()
	{
		$cities = Cities::find()->all();
		$data = [];

		if(Yii::$app->request->post('city')) {
			$city_id = Yii::$app->request->post('city');

			$offers = Offers::findAll(['city' => $city_id]);

			$offers_ids = ArrayHelper::getColumn($offers, 'id');

			$data['Город'] = Cities::findOne(['id' => $city_id])->city;
			$data['Количество сделок'] = count($offers);

			$services = Services::findAll(['offer_id' => $offers_ids ]);

			$data['Количество услуг'] = count($services);

			$payments = Payments::findAll(['service_id' => ArrayHelper::getColumn($services, 'id') ]);

			$data['Количество платежей'] = count($payments);

			$actions = Action::findAll(['offer_id' => $offers_ids ]);

			$data['Количество действий (встреч, звонков)'] = count($actions);

			$history = History::findAll(['offer_id' => $offers_ids ]);

			$data['Количество записей в истории'] = count($history);

			$comments = Comments::findAll(['offer_id' => $offers_ids ]);

			$data['Количество комментариев'] = count($comments);

			$addresses = Address::findAll(['city_id' => $city_id ]);

			$data['Количество адресов'] = count($addresses);

			if(Yii::$app->request->post('delete')) {
				$data['Удалено сделок'] = Offers::deleteAll(['id' => $offers_ids]);
				$data['Удалено услуг'] = Services::deleteAll(['id' => ArrayHelper::getColumn($services, 'id')]);
				$data['Удалено платежей'] = Payments::deleteAll(['id' => ArrayHelper::getColumn($payments, 'id')]);
				$data['Удалено действий'] = Action::deleteAll(['id' => ArrayHelper::getColumn($actions, 'id')]);
				$data['Удалено записей в истории'] = History::deleteAll(['id' => ArrayHelper::getColumn($history, 'id')]);
				$data['Удалено комментариев'] = Comments::deleteAll(['id' => ArrayHelper::getColumn($comments, 'id')]);
				$data['Удалено адресов'] = Address::deleteAll(['id' => ArrayHelper::getColumn($addresses, 'id')]);
			}
		}

		return $this->render('delete-city', [
			'cities' => $cities,
			'data' => $data
		]);
	}

    /**
     * Finds the cities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return cities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = cities::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
