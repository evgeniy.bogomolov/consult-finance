<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 12.06.2019
 * Time: 18:42
 */
?>
<div class="offer-task">
    <div class="row">
        <div class="col-md-2">
            <?php
                switch ($model->name){
                    case 'meeting':
                        echo 'Встреча';
                        break;
                    case 'callback':
                        echo 'Перезвонить';
                        break;
                }
            ?>
        </div>
        <div class="col-md-5">
            <?= date('d.m.Y H:i', strtotime($model->datetime)); ?>
        </div>
        <div class="col-md-5">
            <?= $model->address; ?>
        </div>
    </div>
</div>
