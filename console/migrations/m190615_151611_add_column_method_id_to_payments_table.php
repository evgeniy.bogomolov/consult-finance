<?php

use yii\db\Migration;

/**
 * Class m190615_151611_add_column_method_id_to_payments_table
 */
class m190615_151611_add_column_method_id_to_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%payments}}', 'method_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190615_151611_add_column_method_id_to_payments_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190615_151611_add_column_method_id_to_payments_table cannot be reverted.\n";

        return false;
    }
    */
}
