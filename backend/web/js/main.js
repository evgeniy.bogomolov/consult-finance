/**
 * Created by g on 05.05.2019.
 */
/*var start = moment().subtract(29, 'days');
var end = moment();

function cb(start, end) {
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}

$('#reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, cb);*/

//cb(start, end);


$('#date').daterangepicker({
    "locale": {
        "format": "DD.MM.YYYY",
        "separator": " - ",
        "applyLabel": "Применить",
        "cancelLabel": "Отменить",
        "fromLabel": "От",
        "toLabel": "До",
        "weekLabel": "Н",
        "customRangeLabel": "Выбрать период",
        "daysOfWeek": [
            "Вс",
            "Пн",
            "Вт",
            "Ср",
            "Чт",
            "Пт",
            "Сб"
        ],
        "monthNames": [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        ],
    },
    autoUpdateInput: false,
    ranges: {
        'Сегодня': [moment(), moment()],
        'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'За последние 7 дней': [moment().subtract(6, 'days'), moment()],
        'За последние 30 дней': [moment().subtract(29, 'days'), moment()],
        'В этом месяце': [moment().startOf('month'), moment().endOf('month')],
        'В предыдущем месяце': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'За всё время': [moment('1970-01-01'), moment()]
    }
}, function(start_date, end_date) {

});

$('#date').on('apply.daterangepicker', function(ev, picker) {
    start_date = picker.startDate.format('DD.MM.YYYY');
    end_date = picker.endDate.format('DD.MM.YYYY');
    $(this).val(start_date+' - '+end_date);
});


$('#ring-time, #meet-time').daterangepicker({
    singleDatePicker: true,
    timePicker: true,
    timePicker24Hour: true,
    "locale": {
        "format": "DD.MM.YYYY H:mm",
        "separator": " - ",
        "applyLabel": "Применить",
        "cancelLabel": "Отменить",
        "fromLabel": "От",
        "toLabel": "До",
        "weekLabel": "Н",
        "daysOfWeek": [
            "Вс",
            "Пн",
            "Вт",
            "Ср",
            "Чт",
            "Пт",
            "Сб"
        ],
        "monthNames": [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        ],
    }
});

/*$('#contact').autocomplete({
    minChars: 5, // Минимальная длина запроса для срабатывания автозаполнения
    delimiter: /(,|;)\s*!/, // Разделитель для нескольких запросов, символ или регулярное выражение
    maxHeight: 400, // Максимальная высота списка подсказок, в пикселях
    width: 300, // Ширина списка
    zIndex: 9999, // z-index списка
    deferRequestBy: 300, // Задержка запроса (мсек), на случай, если мы не хотим слать миллион запросов, пока пользователь печатает. Я обычно ставлю 300.
    params: { country: 'Yes'}, // Дополнительные параметры
    onSelect: function(data, value){ }, // Callback функция, срабатывающая на выбор одного из предложенных вариантов,
    lookup: ['79999999999', '79999999998', '79999999997'] // Список вариантов для локального автозаполнения
});*/


$(".departments-table .show-deals").click(function () {
    $(".department-deals").modal('show');
});

$(".emp-table .show-deals").click(function () {
    $(".emp-deals").modal('show');
});

$("body").on('click', '.add-payment-button', function (e) {
    e.preventDefault();
    $(this).closest('.offer-service').find(".add-payment").modal('show');
});

$(".add-service-button").click(function (e) {
    e.preventDefault();
    $(".add-service").modal('show');
});

$(".service-amount-update-button").click(function (e) {
    e.preventDefault();
    $(".service-amount-update").modal('show');
});

$(".department-filter").bind("click", function () {
   $(".main-table").hide();
   $(".emp-table").hide();
   $(".departments-table").show();
});

$(".employee-filter").bind("click", function () {
    $(".main-table").hide();
    $(".emp-table").show();
    $(".departments-table").hide();
});

$('#file').bootstrapFileInput();

$('body').on("click",'.add_payment_button',function (e) {
    e.preventDefault();
    form = $(this).closest('.add-payment');
    paid_summ_block = $(form).parent('.offer-service').find('.paid_text');


    summ = form.find("[name=summ]").val();
    service_id = form.find("[name=service_id]").val();
    city_id = form.find("[name=city_id]").val();
    method_id = form.find("[name=method_id]").val();



    $.ajax({
        url: "/offers/add-payment?service_id="+service_id+"&summ="+summ+"&city_id="+city_id+"&method_id="+method_id,
        success: function(respond){
            if(respond == 'Ошибка') {
                alert('Ошибка');
            }
            $(paid_summ_block).text(respond);
            $.notify("Платеж добавлен", "success");
            form.modal('hide');
        }
    });
});


$(".add_service_button").on('click', function (e) {
    e.preventDefault();
    form = $(this).closest('.add_service_form');
    modal = $(this).closest('.add-service');
    data = form.serialize();

    $.ajax({
        url: "/offers/add-service",
        method: 'POST',
        data: data,
        success: function(respond){
            location.reload();
            $.notify("Услуга добавлена", "success");
            $(".offer-services-list").append(respond);
            modal.modal('hide');
        }
    });
});

$(".amount_update_button").on('click', function (e) {
    e.preventDefault();
    form = $(this).closest('.service-amount-update');
    new_summ = form.find("[name=new_summ]").val();
    service_id = form.find("[name=service_id]").val();
    amount_summ_text = $(form).parent('.offer-service').find('.offer-service-amount-text');

    $.ajax({
        url: "/offers/service-amount-update?service_id="+service_id+"&new_summ="+new_summ,
        success: function(respond){
            if(respond == 'Ошибка') {
                $.notify("Ошибка", "error");
            }
            else {
                $(amount_summ_text).text(respond);
                $.notify("Стоимость изменена", "success");
            }
            form.modal('hide');
        }
    });
});

$(".add_action").on("submit", function (e) {
    e.preventDefault();
});
$(".add_action_button").on('click', function (e) {
    e.preventDefault();
    form = $(this).closest(".add_action");
    data = form.serialize();

    $.ajax({
        url: "/offers/add-action",
        method: 'POST',
        data: data,
        success: function(respond){
            $('.offer-task-list').prepend(respond);
        }
    });
});

$(".be-responsible").click(function (e) {
    e.preventDefault();
    offer_id = $(this).data("offer-id");
    select = $("[name='Offers[responsible]']");
    $.ajax({
        url: "/offers/change-responsible?offer_id="+offer_id,
        method: 'POST',
        success: function(respond){
            respond = JSON.parse(respond);
            $.notify(respond["message"], "success");

            if(respond["responsible_id"]) {
                select.val(respond["responsible_id"]);
            }
        }
    });
});

$(".haveAcceptButton").on("change", function () {
   $(this).parent().parent().find(".accept-change").show();
});

$(".accept-change").click(function () {
   value = $(this).prev().find("select").serialize();
   offer_id = $(this).data("offer-id");
    $.ajax({
        url: "/offers/change-offer-feature?offer_id="+offer_id,
        method: 'POST',
        data: value,
        success: function(respond){
            $.notify(respond, "success");
        }
    });
});

$(".show-department-payments").click(function () {
    url = $(this).attr("href");
    modal_body = $(".department-deals .modal-body");

    $.ajax({
        url: url,
        method: 'POST',
        beforeSend: function () {
            modal_body.html();
        },
        success: function(respond){
            $(".department-deals").modal('show');
            modal_body.html(respond);
        }
    });
});

$(".show-department-payments-by-employee").click(function () {
    url = $(this).attr("href");
    modal_body = $(".department-deals-by-employee .modal-body");

    $.ajax({
        url: url,
        method: 'POST',
        beforeSend: function () {
            //modal_body.html();
        },
        success: function(respond){
            console.log("YE");
            $(".department-deals-by-employee").modal('show');
            modal_body.html(respond);
        }
    });
});

$(".show-employee-payments").click(function () {
    url = $(this).attr("href");
    modal_body = $(".emp-deals .modal-body");

    $.ajax({
        url: url,
        method: 'POST',
        success: function(respond){
            $(".emp-deals").modal('show');
            modal_body.html(respond);
        }
    });
});

$(".view-all-payments").click(function (e) {
    e.preventDefault();
    url = $(this).attr("href");
    modal_body = $(".all-payments .modal-body");

    $.ajax({
        url: url,
        success: function(respond){
            $(".all-payments").modal('show');
            modal_body.html(respond);
        }
    });
});

$(".all-payments").on('click', '.cancel-payment-btn', function (e) {
    e.preventDefault();
    url = $(this).attr("href");
    modal_body = $(".all-payments .modal-body");

    $.ajax({
        url: url,
        success: function(respond){
            modal_body.find("#payment-"+respond).remove();
        }
    });
});
$(".status_offers_btn").click(function (e) {
    e.preventDefault();
    url = $(this).attr("href");
    data = $(this).data('offers');
    modal_body = $(".status__offers .modal-body");

    $.ajax({
        url: url,
        method: 'GET',
        data: {offers_ids:data},
        success: function(respond){
            $(".status__offers").modal('show');
            modal_body.html(respond);
        }
    });
});



$("[name='Clients[phone]']").inputmask("+7(999)999-99-99");
$("[name='SignupForm[phone]'], #user-phone").inputmask("+7(999)999-99-99");
$("[name='SignupForm[birthday]'], #user-birthday").inputmask("99.99.9999");


$(document).ready(function() {
    $(".fancybox").fancybox();
});

$('#delete-form').on('beforeSubmit', function (e) {
    if (!confirm("Вы уверены? Данные будут удалены безвозвратно.")) {
        return false;
    }
    return true;
});