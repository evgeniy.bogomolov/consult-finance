<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Добавить пользователя';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'fullname') ?>
                <?= $form->field($model, 'phone') ?>
                <?= $form->field($model, 'birthday')->textInput(["placeholder" => "дд.мм.гггг"]) ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'role')->dropDownList($roles); ?>

                <?=
                $form->field($model, 'cities_ids')->checkboxList($cities);
                ?>

                <?= $form->field($model, 'department_id')->dropDownList($departments, ["prompt" => "Выберите отдел"]) ?>
                <?= $form->field($model, 'password')->passwordInput() ?>


                <div class="form-group">
                    <?= Html::submitButton('Зарегистрировать', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
