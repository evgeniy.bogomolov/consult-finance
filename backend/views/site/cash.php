<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 07.05.2019
 * Time: 20:11
 */
$this->title = "Касса";
?>

<div class="offers-filters">
    <div class="container-fluid no-paddings">
        <div class="row">
            <div class="col-md-2">
                <label for="">
                    Город
                </label>
                <select class="form-control">
                    <option>Новосибирск</option>
                    <option>Самара</option>
                </select>
            </div>
            <div class="col-md-2">
                <label for="" >
                    Отдел
                </label>
                <select class="form-control">
                    <option>Все</option>
                    <option>ОП</option>
                    <option>ОИ</option>
                </select>
            </div>
            <div class="col-md-2">
                <label for="">
                    Сотрудник
                </label>
                <select class="form-control">
                    <option>Все</option>
                    <option>Иванов Иван</option>
                    <option>Иванов Иван</option>
                    <option>Иванов Иван</option>
                    <option>Иванов Иван</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="">
                    Период
                </label>
                <div class="form-group">
                    <input id="date" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <input type="submit" class="btn btn-success" value="Применить" style="margin-top: 24px">
            </div>
        </div>

    </div>
</div>

<div class="kind-filter">
    <button class="department-filter btn btn-success">По отделам</button>
    <button class="employee-filter btn btn-success">По сотрудникам</button>

</div>

<div class="cash">
    <table class="table main-table table-bordered">
        <thead class="thead">
        <tr>
            <td>
                Дата
            </td>
            <td>
                Сумма
            </td>
            <td>
                Сотрудник
            </td>
            <td>
                Отдел
            </td>
            <td>
                Город
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                01.01.2018 12:34
            </td>
            <td>
                7 000
            </td>
            <td>
                Иванов Иван Иванович
            </td>
            <td>
                ОП
            </td>
            <td>
                Новосибирск
            </td>
        </tr>
        <tr>
            <td>
                01.01.2018 12:34
            </td>
            <td>
                15 000
            </td>
            <td>
                Иванов Иван Иванович
            </td>
            <td>
                ОП
            </td>
            <td>
                Новосибирск
            </td>
        </tr>
        <tr>
            <td>
                01.01.2018 12:34
            </td>
            <td>
                25 000
            </td>
            <td>
                Иванов Иван Иванович
            </td>
            <td>
                ОИ
            </td>
            <td>
                Самара
            </td>
        </tr>
        <tr>
            <td>
                01.01.2018 12:34
            </td>
            <td>
                50 000
            </td>
            <td>
                Иванов Иван Иванович
            </td>
            <td>
                ОП
            </td>
            <td>
                Новосибирск
            </td>
        </tr>

        </tbody>
    </table>

    <table class="table departments-table table-bordered">
        <thead class="thead">
            <tr>
                <td colspan="5">10.01.2019 - 10.02.2019, Новосибирск</td>
            </tr>
            <tr>
                <td>
                    Отдел
                </td>
                <td>
                    Сумма
                </td>
                <td>
                    Количество сделок
                </td>
                <td>
                    Способы оплаты
                </td>
                <td>
                    Действия
                </td>
            </tr>
        </thead>
        <tbody>
        <tr>
            <div>
            <td>
                ОП
            </td>
            <td>
                1 000 000
            </td>
            <td>
                50
            </td>
            <td>
                500 000/200 000/300 000
            </td>
            <td>
                <a href="#" class="show-deals" data-toggle="modal" data-target="#department-deals">Показать сделки</a>
            </td>
            </div>
        </tr>
        <tr class="child">
            <td colspan="5">
                <table class="table table-bordered">
                    <tr>
                        <td>Сотрудник</td>
                        <td>Сумма</td>
                        <td>Количество сделок</td>
                        <td>Способы оплаты</td>
                    </tr>
                    <tr>
                        <td>Иванов Иван</td>
                        <td>10 000</td>
                        <td>5</td>
                        <td>3000/2000/5000</td>
                    </tr>
                    <tr>
                        <td>Иванов Иван</td>
                        <td>10 000</td>
                        <td>5</td>
                        <td>3000/2000/5000</td>
                    </tr>
                    <tr>
                        <td>Иванов Иван</td>
                        <td>10 000</td>
                        <td>5</td>
                        <td>3000/2000/5000</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                ОИ
            </td>
            <td>
                1 000 000
            </td>
            <td>
                50
            </td>
            <td>
                500 000/200 000/300 000
            </td>
            <td>
                <a href="#" class="show-deals" data-toggle="modal" data-target="#department-deals">Показать сделки</a>
            </td>
        </tr>
        <tr class="child">
            <td colspan="5">
                <table class="table table-bordered">
                    <tr>
                        <td>Сотрудник</td>
                        <td>Сумма</td>
                        <td>Количество сделок</td>
                        <td>Способы оплаты</td>
                    </tr>
                    <tr>
                        <td>Иванов Иван</td>
                        <td>10 000</td>
                        <td>5</td>
                        <td>3000/2000/5000</td>
                    </tr>
                    <tr>
                        <td>Иванов Иван</td>
                        <td>10 000</td>
                        <td>5</td>
                        <td>3000/2000/5000</td>
                    </tr>
                    <tr>
                        <td>Иванов Иван</td>
                        <td>10 000</td>
                        <td>5</td>
                        <td>3000/2000/5000</td>
                    </tr>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="table emp-table table-bordered">
        <thead class="thead">
            <tr>
                <td colspan="6">10.01.2019 - 10.02.2019, Новосибирск</td>
            </tr>
            <tr>
                <td>
                    Сотрудник
                </td>
                <td>
                    Отдел
                </td>
                <td>
                    Сумма
                </td>
                <td>
                    Количество сделок
                </td>
                <td>
                    Способы оплаты
                </td>
                <td>
                    Действия
                </td>
            </tr>
        </thead>
        <tbody>
        <tr>
                <td>
                    Иванов Иван
                </td>
                <td>
                    ОП
                </td>
                <td>
                    1 000
                </td>
                <td>
                    50
                </td>
                <td>
                    1 000/0/0
                </td>
                <td>
                    <a href="#" class="show-deals" data-toggle="modal">Показать сделки</a>
                </td>
        </tr>
        <tr>
            <td>
                Иванов Иван
            </td>
            <td>
                ОИ
            </td>
            <td>
                30 000
            </td>
            <td>
                50
            </td>
            <td>
                10 000/40 000/0
            </td>
            <td>
                <a href="#" class="show-deals" data-toggle="modal">Показать сделки</a>
            </td>
        </tr>

        </tbody>
    </table>

    <div class="department-deals modal fade">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Сделки отдела</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead class="thead">
                    <tr>
                        <td>
                            Дата
                        </td>
                        <td>
                            Сумма
                        </td>
                        <td>
                            Контакт
                        </td>
                        <td>
                            Услуга
                        </td>
                        <td>
                            Сотрудник
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                12.12.2010
                            </td>
                            <td>
                                20 000
                            </td>
                            <td>
                                Иванов Иван
                            </td>
                            <td>
                                Личный брокер
                            </td>
                            <td>
                                Иванов Иван
                            </td>
                        </tr>
                        <tr>
                            <td>
                                12.12.2010
                            </td>
                            <td>
                                20 000
                            </td>
                            <td>
                                Иванов Иван
                            </td>
                            <td>
                                Личный брокер
                            </td>
                            <td>
                                Иванов Иван
                            </td>
                        </tr>
                        <tr>
                            <td>
                                12.12.2010
                            </td>
                            <td>
                                20 000
                            </td>
                            <td>
                                Иванов Иван
                            </td>
                            <td>
                                Личный брокер
                            </td>
                            <td>
                                Иванов Иван
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>

    <div class="emp-deals modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Сделки сотрудника</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead class="thead">
                        <tr>
                            <td>
                                Дата
                            </td>
                            <td>
                                Сумма
                            </td>
                            <td>
                                Контакт
                            </td>
                            <td>
                                Услуга
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                12.12.2010
                            </td>
                            <td>
                                20 000
                            </td>
                            <td>
                                Иванов Иван
                            </td>
                            <td>
                                Личный брокер
                            </td>
                        </tr>
                        <tr>
                            <td>
                                12.12.2010
                            </td>
                            <td>
                                20 000
                            </td>
                            <td>
                                Иванов Иван
                            </td>
                            <td>
                                Личный брокер
                            </td>
                        </tr>
                        <tr>
                            <td>
                                12.12.2010
                            </td>
                            <td>
                                20 000
                            </td>
                            <td>
                                Иванов Иван
                            </td>
                            <td>
                                Личный брокер
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>