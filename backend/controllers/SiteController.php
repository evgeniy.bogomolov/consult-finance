<?php
namespace backend\controllers;

use backend\models\Cities;
use backend\models\Clients;
use backend\models\Departments;
use backend\models\Offers;
use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\SignupForm;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('Manager')) {
            return $this->redirect("clients/index");
        }
        return $this->redirect("offers/index");
    }

    /**
     * @return string
     */


    public function actionCash()
    {
        return $this->render('cash');
    }


    public function actionUsers()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->orderBy(['status' => SORT_DESC]),
        ]);
        return $this->render('users',[
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Пользователь зарегистрирован');
            return $this->redirect("/site/users");
        }
        $roles = ArrayHelper::map(Yii::$app->getAuthManager()->getRoles(), 'name', 'description');
        $cities = ArrayHelper::map(Cities::find()->all(), 'id', 'city');
        $departments = ArrayHelper::map(Departments::find()->all(), 'id', 'name');
        return $this->render('signup', [
            'model' => $model,
            'roles' => $roles,
            'cities' => $cities,
            'departments' => $departments
        ]);
    }

    public function actionCityDepartments($id) {
        $data = [];
        $data .= "<option value='6'>КЦ</option>";

        if($items = Departments::find()->where(['city_id'=>$id])->all()){
            foreach($items as $item){
                $data .= "<option value='".$item->id."'>".$item->name."</option>";
            }
        }
        return $data;
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
