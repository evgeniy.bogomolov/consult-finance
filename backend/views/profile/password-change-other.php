<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


$this->title = 'Смена пароля пользователя '.$model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Профиль', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-profile-password-change">
    <div class="row">
    <div class="user-form col-md-6">

        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group">
            <label for="">Новый пароль</label>
            <input type="text" class="form-control" name="new_password" placeholder="Введите новый пароль">
        </div>


        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    </div>
</div>