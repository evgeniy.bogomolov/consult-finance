<?php

use yii\db\Migration;

/**
 * Class m191102_164520_add_updated_at_to_offers
 */
class m191102_164520_add_updated_at_to_offers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('{{%offers}}', 'updated_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191102_164520_add_updated_at_to_offers cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191102_164520_add_updated_at_to_offers cannot be reverted.\n";

        return false;
    }
    */
}
