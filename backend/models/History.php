<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "history".
 *
 * @property int $id
 * @property string $date
 * @property int $offer_id
 * @property string $action
 * @property int $user_id
 * @property int $payment_id
 * @property int $service_id
 * @property int $old_department_id
 * @property int $new_department_id
 * @property int $old_status_id
 * @property int $new_status_id
 * @property int $old_responsible_id
 * @property int $new_responsible_id
 * @property int $payment
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history';
    }

    public function behaviors(){
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                // если вместо метки времени UNIX используется datetime:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['offer_id', 'user_id', 'payment_id', 'service_id', 'old_department_id', 'new_department_id', 'old_status_id', 'new_status_id', 'old_responsible_id', 'new_responsible_id'], 'integer'],
            [['action'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'offer_id' => 'Offer ID',
            'action' => 'Action',
            'user_id' => 'User ID',
            'payment_id' => 'Payment ID',
            'service_id' => 'Service ID',
            'department_id' => 'Department ID',
            'old_status_id' => 'Old Status ID',
            'new_status_id' => 'New Status ID',
            'old_responsible_id' => 'Old Responsible ID',
            'new_responsible_id' => 'New Responsible ID',
        ];
    }

    public function getOldDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'old_department_id']);
    }

    public function getNewDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'new_department_id']);
    }
    public function getOldStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'old_status_id']);
    }

    public function getNewStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'new_status_id']);
    }
    public function getOldResponsible()
    {
        return $this->hasOne(User::className(), ['id' => 'old_responsible_id']);
    }
    public function getNewResponsible()
    {
        return $this->hasOne(User::className(), ['id' => 'new_responsible_id']);
    }
    public function getPayment()
    {
        return $this->hasOne(Payments::className(), ['id' => 'payment_id']);
    }
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
