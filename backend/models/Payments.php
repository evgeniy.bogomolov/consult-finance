<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use backend\models\History as OfferHistory;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $summ
 * @property int $user_id
 * @property int $department_id
 * @property int $service_id
 * @property string $date
 * @property string $city
 * @property string $service
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    public function afterSave($insert, $changedAttributes)
    {

        $history = new OfferHistory();
        $offer_id = $this->service->offer_id;
        $history->offer_id = $offer_id;
        $history->user_id = Yii::$app->user->identity->id;

        $history->payment_id = $this->id;
        $history->service_id = $this->service_id;
        $history->action = "add_payment";

        $history->save();

        parent::afterSave($insert, $changedAttributes);
    }

    public function behaviors(){
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['summ', 'user_id', 'department_id', 'service_id', 'city_id', 'method_id'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'summ' => 'Сумма',
            'method_id' => 'Способ оплаты',
            'user_id' => 'Ответственный',
            'department_id' => 'Отдел',
            'service_id' => 'Услуга',
            'city_id' => 'Город',
            'date' => 'Дата'
        ];
    }

    static function paymentsByDepartmentsAndDate($city_id, $date_period) {
        $departments = Departments::find()->all();
        $methods = Method::find()->all();

        $date_period = explode(" - ", $date_period);
        $date_from = $date_period[0];
        $date_to = $date_period[1];

        $createdDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');

        $createdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to . ' 23:59:59');

        $payments_by_departments = [];

        foreach ($departments as $department) {
            $payments_by_departments[$department->name]['department_id'] = $department->id;
            $payments = Payments::find()
                ->where([
                    "department_id"=>$department->id,
                    "city_id" => $city_id
                ])
                ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
                ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null]);

            $payments_by_departments[$department->name]['summ'] = $payments->sum('summ');

            $payments_by_departments[$department->name]['count'] = $payments->count();

            foreach ($methods as $method) {
                $payments_by_departments[$department->name]['methods'][$method->name] = Payments::find()
                    ->where(["department_id"=>$department->id, "method_id" => $method->id])
                    ->andFilterWhere(["city_id" => $city_id])
                    ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
                    ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null])
                    ->sum('summ');

            }
        }

        return $payments_by_departments;
    }

    static function paymentsByDepartments() {
        $departments = Departments::find()->all();
        $methods = Method::find()->all();

        $payments_by_departments = [];

        foreach ($departments as $department) {
            $payments_by_departments[$department->name]['department_id'] = $department->id;
            $payments_by_departments[$department->name]['summ'] = Payments::find()->where(["department_id"=>$department->id])->sum('summ');
            $payments_by_departments[$department->name]['count'] = Payments::find()->where(["department_id"=>$department->id])->count();

            foreach ($methods as $method) {
                $payments_by_departments[$department->name]['methods'][$method->name] = Payments::find()->where(["department_id"=>$department->id, "method_id" => $method->id])->sum('summ');;

            }
        }

        return $payments_by_departments;
    }

    static function paymentsByDepartmentsByEmployee($city_id, $department_id, $date_period) {
        $employee = User::find()
            ->joinWith('cities')
            ->where([
                "department_id"=>$department_id,
                "cities.id" => $city_id
            ])
            ->all();

        $methods = Method::find()->all();

        $date_period = explode(" - ", $date_period);
        $date_from = $date_period[0];
        $date_to = $date_period[1];

        $createdDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');
        $createdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to . ' 23:59:59');

        $payments_by_employee = [];

        foreach ($employee as $user) {

            $payments =  Payments::find()
                ->where([
                    "user_id"=>$user->id,
                    "city_id" => $city_id
                ])
                ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
                ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null]);

            $payments_by_employee[$user->fullname]['summ'] = $payments->sum('summ');
            $payments_by_employee[$user->fullname]['count'] = $payments->count();

            foreach ($methods as $method) {
                $payments_by_employee[$user->fullname]['methods'][$method->name] = $payments
                    ->where([
                        "method_id"=>$method->id,
                        "user_id" => $user->id
                    ])
                    ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
                    ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null])
                    ->sum('summ');

            }
            //var_dump($payments_by_employee);
        }

        return $payments_by_employee;
    }

    static function paymentsByEmployee() {
        $users = User::find()->all();
        $methods = Method::find()->all();

        $payments_by_employee = [];

        foreach ($users as $user) {
            $payments_by_employee[$user->fullname]['user_id'] = $user->id;
            $payments_by_employee[$user->fullname]['user_name'] = $user->fullname;
            $payments_by_employee[$user->fullname]['department'] = $user->department->name;
            $payments_by_employee[$user->fullname]['summ'] = Payments::find()->where(["user_id"=>$user->id])->sum('summ');
            $payments_by_employee[$user->fullname]['count'] = Payments::find()->where(["user_id"=>$user->id])->count();

            foreach ($methods as $method) {
                $payments_by_employee[$user->fullname]['methods'][$method->name] = Payments::find()->where(["user_id"=>$user->id, "method_id" => $method->id])->sum('summ');;

            }
        }

        return $payments_by_employee;
    }

    static function paymentsByEmployeeAndDate($city_id, $date_period) {
        $users = User::find()->all();
        $methods = Method::find()->all();

        $date_period = explode(" - ", $date_period);
        $date_from = $date_period[0];
        $date_to = $date_period[1];

        $createdDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');

        $createdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to . ' 23:59:59');

        $payments_by_employee = [];

        foreach ($users as $user) {
            $payments_by_employee[$user->fullname]['user_id'] = $user->id;
            $payments_by_employee[$user->fullname]['user_name'] = $user->fullname;
            $payments_by_employee[$user->fullname]['department'] = $user->department->name;
            $payments = Payments::find()
                ->where([
                    "user_id"=>$user->id,
                    "city_id" => $city_id
                ])
                ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
                ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null]);

            $payments_by_employee[$user->fullname]['summ'] = $payments->sum('summ');

            $payments_by_employee[$user->fullname]['count'] = $payments->count();

            foreach ($methods as $method) {
                $payments_by_employee[$user->fullname]['methods'][$method->name] = $payments->andFilterWhere(
                    ["method_id"=>$method->id]
                )->sum('summ');

            }
        }

        return $payments_by_employee;
    }

    static function paymentsByEmployeeList($user_id, $date_period) {
        $user = User::find()->where(["id"=>$user_id])->one();
        $methods = Method::find()->all();

        $date_period = explode(" - ", $date_period);
        $date_from = $date_period[0];
        $date_to = $date_period[1];

        $createdDateFrom = \DateTime::createFromFormat('d.m.Y H:i:s', $date_from. ' 00:00:00');
        $createdDateTo = \DateTime::createFromFormat('d.m.Y H:i:s', $date_to . ' 23:59:59');

        $payments_by_employee = [];

        $payments =  Payments::find()
            ->where([
                "user_id"=>$user_id,
            ])
            ->andFilterWhere(['>=', 'date', $date_from ? $createdDateFrom->format('Y-m-d H:i:s') : null])
            ->andFilterWhere(['<=', 'date', $date_to ? $createdDateTo->format('Y-m-d H:i:s') : null])
            ->all();

        $n = 0;
        foreach ($payments as $payment) {
            $payments_by_employee[$n]['date'] = $payment->date;
            $payments_by_employee[$n]['summ'] = $payment->summ;
            $payments_by_employee[$n]['method'] = $payment->method->name;
            $payments_by_employee[$n]['client'] = $payment->service->offer->client->name;
            $payments_by_employee[$n]['service'] = $payment->service->name;
            $n++;
        }



        return $payments_by_employee;
    }

    public function periodToTwoDates() {

    }

    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    public function getResponsible()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }

    public function getMethod()
    {
        return $this->hasOne(Method::className(), ['id' => 'method_id']);
    }
}
