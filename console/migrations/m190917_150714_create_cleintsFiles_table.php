<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cleintsFiles}}`.
 */
class m190917_150714_create_cleintsFiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%clientsFiles}}', [
            'id' => $this->primaryKey(),
            'file' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%clientsFiles}}');
    }
}
