<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Касса';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php echo $this->render('_search-departments', [
    'cities' => $cities,
    'filters' => $filters
]);
$city_id = Yii::$app->request->get("Filters");
$city_id = $city_id['city_id'];

?>

<div class="cash">
    <div class="cash-wrap box-body table-responsive no-padding">
    <table class="table departments-table table-bordered">
        <thead class="thead">
        <?if(Yii::$app->request->get("Filters")):?>
        <tr>
            <td colspan="5">
                <?= $filters['date'] ?>, <?= $filters['city'] ?>

            </td>
        </tr>
        <?endif;?>
        <tr>
            <td>
                Отдел
            </td>
            <td>
                Сумма
            </td>
            <td>
                Количество сделок
            </td>
            <td>
                Способы оплаты
            </td>
            <td>
                Действия
            </td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($payments as $key => $value): ?>
            <tr data-url="/payments/department-deals-by-employee?department_id=<?= $value['department_id'] ?>&date_period=<?= $filters['date'] ?>" class="department-tr">
                <td>
                    <?= $key ?>
                </td>
                <td>
                    <?= number_format($value['summ'], 0, '', ' ') ?>
                </td>
                <td>
                    <?= $value['count'] ?>
                </td>
                <td>
                    <?php foreach ($value['methods'] as $method_name => $method_summ){ ?>
                        <?= $method_name ?>: <?= number_format($method_summ, 0, '', ' ')?> <br>
                    <?php } ?>
                </td>
                <td>
                    <a href="/payments/department-deals?city_id=<?= $city_id ?>&department_id=<?= $value['department_id'] ?>&date_period=<?= $filters['date'] ?>" class="show-department-payments" data-toggle="modal" data-target="#department-deals">Показать сделки</a>
                    <br>
                    <a href="/payments/department-deals-by-employee?city_id=<?= $city_id ?>&department_id=<?= $value['department_id'] ?>&date_period=<?= $filters['date'] ?>" class="show-department-payments-by-employee" data-toggle="modal" data-target="#department-deals-by-employee">По сотрудникам</a>
                </td>

            </tr>
        <?endforeach;?>
        <tr class="child">
            <td colspan="5">
                <table class="table table-bordered">
                    <tr>
                        <td>Сотрудник</td>
                        <td>Сумма</td>
                        <td>Количество сделок</td>
                        <td>Способы оплаты</td>
                    </tr>
                    <tr>
                        <td>Иванов Иван</td>
                        <td>10 000</td>
                        <td>5</td>
                        <td>3000/2000/5000</td>
                    </tr>
                    <tr>
                        <td>Иванов Иван</td>
                        <td>10 000</td>
                        <td>5</td>
                        <td>3000/2000/5000</td>
                    </tr>
                    <tr>
                        <td>Иванов Иван</td>
                        <td>10 000</td>
                        <td>5</td>
                        <td>3000/2000/5000</td>
                    </tr>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    </div>
    <div class="department-deals modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Сделки отдела</h4>
                </div>
                <div class="modal-body box-body table-responsive no-padding">

                </div>
            </div>
        </div>
    </div>

    <div class="department-deals-by-employee modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Сделки отдела</h4>
                </div>
                <div class="modal-body box-body table-responsive no-padding">

                </div>
            </div>
        </div>
    </div>
</div>
