<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%services}}`.
 */
class m190611_160135_create_services_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%services}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'cost' => $this->integer(),
            'department_id'=> $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%services}}');
    }
}
