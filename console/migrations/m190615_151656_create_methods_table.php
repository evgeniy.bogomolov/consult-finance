<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%methods}}`.
 */
class m190615_151656_create_methods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%methods}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%methods}}');
    }
}
