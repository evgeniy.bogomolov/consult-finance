<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\status */

$this->title = 'Изменить статус: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Статусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="status-update">

    <?= $this->render('_form', [
        'model' => $model,
        'departments' => $departments
    ]) ?>

</div>
