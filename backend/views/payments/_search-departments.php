<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PaymentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'action' => ['departments'],
    'method' => 'get',
]);?>
<div class="offers-filters">
    <div class="container-fluid no-paddings">
        <div class="row">
            <div class="col-md-2">
                <label for="city_id">Город</label>
                <?= Html::dropDownList("Filters[city_id]",$filters['city_id'], $cities,["class"=>"form-control", "promt" => "Все"] ) ?>
            </div>
            <div class="col-md-3">
                <label for="date">Период</label>
                <?= Html::textInput("Filters[date]", $filters['date'], ["class"=>"form-control", "id"=>"date", 'autocomplete' => 'off']) ?>

            </div>
            <div class="col-md-3">
                <div class="form-group" style="padding-top: 24px">
                    <?= Html::submitButton('Применить', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Сброс',['departments'],['class' => 'btn btn-warning']) ?>
                </div>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>

