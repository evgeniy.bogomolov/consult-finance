<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 17.06.2019
 * Time: 19:54
 */
?>
<div class="cash-wrap box-body table-responsive no-padding">
<div class="cash">
    <table class="table table-bordered">
        <thead>
        <tr>
            <td>Сотрудник</td>
            <td>Сумма</td>
            <td>Количество сделок</td>
            <td>Способы оплаты</td>
        </tr>
        </thead>
        <?php foreach ($payments as $key => $value): ?>
            <tr>
                <td><?= $key ?></td>
                <td><?= $value['summ'] ?></td>
                <td><?= $value['count'] ?></td>
                <td>
                    <?php foreach ($value['methods'] as $method_name => $method_summ){ ?>
                        <?=  $method_name ?>: <?= number_format($method_summ, 0, '', ' ') ?> <br>


                    <?php } ?>
                </td>
            </tr>
        <?endforeach;?>
    </table>
</div>
</div>