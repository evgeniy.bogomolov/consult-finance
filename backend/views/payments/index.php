<?php

use backend\models\Payments;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Касса';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash">
    <?php echo $this->render('_search', [
        'model' => $searchModel,
        'filters' => $filters
    ]); ?>
    <div class="cash-wrap box-body table-responsive no-padding">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'Сумма',
                'value' => function($model) {
                    return number_format($model->summ, 0, '', ' ');
                }
            ],
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->responsible->fullname;
                }
            ],
            [
                'attribute' => 'department_id',
                'value' => function($model) {
                    return $model->department->name;
                }
            ],
            [
                'attribute' => 'service_id',
                'value' => function($model) {
                    return $model->service->name;
                }
            ],
            [
                'attribute' => 'city_id',
                'value' => function($model) {
                    return $model->city->city;
                }
            ],
        ],
    ]); ?>
    </div>

</div>
