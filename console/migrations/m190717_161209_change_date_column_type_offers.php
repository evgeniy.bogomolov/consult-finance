<?php

use yii\db\Migration;

/**
 * Class m190717_161209_change_date_column_type_offers
 */
class m190717_161209_change_date_column_type_offers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%offers}}', 'date', 'datetime');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190717_161209_change_date_column_type_offers cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190717_161209_change_date_column_type_offers cannot be reverted.\n";

        return false;
    }
    */
}
