<?php

use yii\db\Migration;

/**
 * Class m190615_141104_add_column_datetime_to_payments_table
 */
class m190615_141104_add_column_datetime_to_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%payments}}', 'date', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190615_141104_add_column_datetime_to_payments_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190615_141104_add_column_datetime_to_payments_table cannot be reverted.\n";

        return false;
    }
    */
}
