<?php

use backend\models\Clients;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\models\offers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(!$isUpdate && Yii::$app->user->can('addContact')): ?>
        <?= $form->field($client, 'name')->textInput()->label("Имя клиента") ?>
        <?= $form->field($client, 'phone')->textInput()->label("Телефон клиента") ?>
    <?php else: ?>
        <?php
            $clients_extended = Clients::find()->all();
            $clients_extended = ArrayHelper::map($clients_extended, 'id', 'namephone');
            echo $form->field($model, 'client_id')->widget(Select2::classname(), [
                'data' => $clients_extended,

            ])->label("Клиент");
        ?>
    <?php endif; ?>
    <?php if(!$isUpdate): ?>
        <?= $form->field($model, 'city')->dropDownList($cities) ?>
    <?php endif; ?>
    <?= Yii::$app->user->can('Manager') ? $form->field($model, 'division')->dropDownList($departments) : "" ?>


    <?= $form->field($model, 'credit_summ')->textInput() ?>
    <?= $form->field($model, 'comment')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
