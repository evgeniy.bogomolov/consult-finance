<?php
/**
 * Created by PhpStorm.
 * User: g
 * Date: 06.08.2019
 * Time: 15:14
 */

?>
<div class="cash-wrap box-body table-responsive no-padding"></div>
<table class="table table-bordered">
    <thead class="thead">
    <tr>
        <td>
            Дата
        </td>
        <td>
            Пользователь
        </td>
        <td>
            Сумма
        </td>
        <?php if(Yii::$app->user->can('cancelPayments')): ?>
        <td>
            Удалить
        </td>
        <?php endif; ?>
    </tr>
    </thead>
    <tbody>
    <?foreach ($payments as $payment):?>
        <tr id="payment-<?= $payment->id ?>">
            <td>
                <?= date('d.m.Y H:m', strtotime($payment->date))?>
            </td>
            <td>
                <?= $payment->responsible->fullname ?>
            </td>
            <td>
                <?= $payment->summ ?>
            </td>
            <?php if(Yii::$app->user->can('cancelPayments')): ?>
            <td>
                <a href="/offers/cancel-payment/?payment_id=<?= $payment->id ?>" class="cancel-payment-btn" title="Удалить платеж" aria-label="Удалить платеж">
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
            </td>
            <?php endif; ?>
        </tr>
    <?endforeach;?>
    </tbody>
</table>
